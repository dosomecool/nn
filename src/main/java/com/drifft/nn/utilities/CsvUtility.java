package com.drifft.nn.utilities;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

public class CsvUtility {

	public static String[] getHeaders(String filename) throws IOException {
		ICsvMapReader mapReader = null;
		String[] headers; 

		try {
			mapReader = new CsvMapReader(new FileReader(filename), CsvPreference.STANDARD_PREFERENCE);
			headers = mapReader.getHeader(true);
		}
		finally {
			if( mapReader != null ) {
				mapReader.close();
			}
		}
		return headers;
	}

	public static CellProcessor[] getProcessors(int columns) {
		CellProcessor[] processors = new CellProcessor[columns];

		for(int i=0; i<columns; i++) {
			processors[i] = new ParseDouble();
		}
		return processors;
	}
	
	public static CellProcessor[] getProcessors(int inputs, int outputs) {
		int columns = inputs + outputs;
		CellProcessor[] processors = new CellProcessor[columns];

		for(int i=0; i<inputs; i++) {
			processors[i] = new ParseDouble();
		}
		
		for(int i=inputs-1; i<outputs; i++) {
			processors[i] =  new Optional(new ParseDouble());
		}
		return processors;
	}


	public static List<Map<String, Object>> readCsvWithHeaders(String filename, CellProcessor[] processors) throws IOException {
		List<Map<String, Object>> data = new ArrayList<>();
		ICsvMapReader mapReader = null;

		try {
			mapReader = new CsvMapReader(new FileReader(filename), CsvPreference.STANDARD_PREFERENCE);
			String[] headers = mapReader.getHeader(true);
			
			Map<String, Object> row;
			while((row = mapReader.read(headers, processors)) != null ) {
				data.add(row);
			}
		}

		finally {
			if( mapReader != null ) {
				mapReader.close();
			}
		}
		return data;
	}
	
	public static void writeWithHeaders(String filename, String[] headers, CellProcessor[] processors, List<Map<String, Object>> mapList) throws IOException {

		ICsvMapWriter mapWriter = null;
		try {
			mapWriter = new CsvMapWriter(new FileWriter(filename),
					CsvPreference.STANDARD_PREFERENCE);

			// Write the header
			mapWriter.writeHeader(headers);
			
			// Write the map
			for(Map<String, Object> map : mapList) {
//				System.out.println(mapList); 
				mapWriter.write(map, headers, processors);
			}
		}
		finally {
			if( mapWriter != null ) {
				mapWriter.close();
			}
		}
	}
}
