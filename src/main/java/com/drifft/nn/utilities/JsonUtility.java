package com.drifft.nn.utilities;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

// Source: https://www.baeldung.com/jackson-object-mapper-tutorial

public class JsonUtility {

	private final static ObjectMapper mapper = new ObjectMapper();
	private final static ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());

	public static Object readJsonToObject(String filename, Class<?> objectClass) 
			throws UnrecognizedPropertyException, InvalidFormatException, 
			JsonMappingException, JsonParseException, IOException  {

		Object object = mapper.readValue(new File(filename), objectClass);
		return object;
	}

	public static void writeObjectToJson(String filename, Object object) 
			throws JsonGenerationException, JsonMappingException, IOException {

		writer.writeValue(new File(filename), object);
	}
}
