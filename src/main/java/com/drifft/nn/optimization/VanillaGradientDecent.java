package com.drifft.nn.optimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drifft.nn.network.NetworkInput.Parameter;
import com.drifft.nn.network.Types.OptimizationType;
import com.drifft.nn.network.Types.ParameterType;

public class VanillaGradientDecent implements Optimizer {

	// Learning rate
	private double etaValue = 0.005;
	private boolean update = false;

	private final Map<String, List<Double>> gradients = new HashMap<String, List<Double>>();

	public VanillaGradientDecent() {
	}

	public VanillaGradientDecent(Double etaValue) {		
		this.etaValue = etaValue;
	}

	public double updateParameter(String id, double weight, double gradient) {	

		// Gradient clipping
		//		if(Math.abs(gradient) > 2.0) {
		//			gradient = 2.0 * (gradient / Math.abs(gradient)); 
		//		}

		if(!gradients.containsKey(id)) {
			gradients.put(id, new ArrayList<Double>());
			gradients.get(id).add(gradient);
		} else {
			gradients.get(id).add(gradient);
		}

		if(update) {
			double sum = 0.0; 

			for(double g : gradients.get(id)) {
				sum = sum + g;
			}
			weight = weight - ( (this.etaValue / gradients.get(id).size()) * sum);
			gradients.get(id).clear();
		}
		return weight;
	}

	public void update(boolean update) {
		this.update = update;
	}

	@Override
	public void setParameters(final List<Parameter> parameterList) {

		parameterList.forEach( parameter -> {

			ParameterType type = parameter.getParameterType();

			switch(type) {
			case ETA: {
				this.etaValue = parameter.getValue();
				break;
			}
			default:
				throw new IllegalArgumentException("Paremeter " + type 
						+ " is not a valid argument for optimization type " + getOptimizationType() + "!");
			}
		});
	}

	@Override
	public OptimizationType getOptimizationType() {
		return OptimizationType.VANILLA;
	}
}
