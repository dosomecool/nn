package com.drifft.nn.optimization;

import com.drifft.nn.network.Types.OptimizationType;

public class OptimizationFactory {

	public OptimizationFactory() {
	}

	public Optimizer get(OptimizationType optimizationType) {

		if(optimizationType == null) {
			return null;
		}

		if(optimizationType.equals(OptimizationType.VANILLA)) {	
			return new VanillaGradientDecent();

		} else if(optimizationType.equals(OptimizationType.MOMENTUM)) {
			return new NesterovMomentum();
		}
		return null;
	}
}
