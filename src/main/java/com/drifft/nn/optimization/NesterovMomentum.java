package com.drifft.nn.optimization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.drifft.nn.network.NetworkInput.Parameter;
import com.drifft.nn.network.Types.OptimizationType;
import com.drifft.nn.network.Types.ParameterType;

public class NesterovMomentum implements Optimizer {

	private double alpha = 0.01;
	private double beta = 0.99;

	// Holds the velocity values
	private final Map<String, Double> history = new HashMap<String, Double>();

	// Holds the gradient value for each weight
	private final Map<String, List<Double>> gradients = new HashMap<String, List<Double>>();
	private boolean update = false;


	public NesterovMomentum() {
	}
	
	public NesterovMomentum(Double alpha, Double beta) {
		this.alpha = alpha;
		this.beta = beta;
	}

	// source: https://distill.pub/2017/momentum/
	// velocity = beta * velocity + gradient
	// weight = weight - alpha * velocity
	public double updateParameter(String id, double weight, double gradient) {

		// gradient clipping
		if(Math.abs(gradient) > 2.0) {
			gradient = 2.0 * (gradient / Math.abs(gradient)); 
		}

		if(!gradients.containsKey(id)) {
			gradients.put(id, new ArrayList<Double>());
			gradients.get(id).add(gradient);
		} else {
			gradients.get(id).add(gradient);
		}

		if(update) {
			double sum = 0.0; 

			for(double g : gradients.get(id)) {
				sum = sum + g;
			}
			Double velocity = history.get(id);
			velocity = (velocity != null) ? beta * velocity + (sum / gradients.get(id).size()) : (sum / gradients.get(id).size());
			weight = weight - alpha * velocity;
			gradients.get(id).clear();
		}
		return weight;
	}

	public void update(boolean update) {
		this.update = update;
	}
	
	@Override
	public void setParameters(final List<Parameter> parameterList) {

		parameterList.forEach( parameter -> {

			ParameterType type = parameter.getParameterType();

			switch(type) {
			case ALPHA: {
				this.alpha = parameter.getValue();
				break;
			}
			case BETA: {
				this.beta = parameter.getValue();
				break;
			}
			default:
				throw new IllegalArgumentException("Paremeter " + type 
						+ " is not a valid argument for optimization type " + getOptimizationType() + "!");
			}
		});
	}

	@Override
	public OptimizationType getOptimizationType() {
		return OptimizationType.MOMENTUM;
	}
}
