package com.drifft.nn.optimization;

import java.util.List;

import com.drifft.nn.network.NetworkInput.Parameter;
import com.drifft.nn.network.Types.OptimizationType;

public interface Optimizer {
 
	public double updateParameter(String id, double weight, double gradient);
	public void update(boolean update);
	public void setParameters(List<Parameter> parameterList);
	public OptimizationType getOptimizationType();
}
