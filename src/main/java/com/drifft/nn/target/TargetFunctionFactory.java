package com.drifft.nn.target;

import com.drifft.nn.network.Types.TargetFunctionType;

public class TargetFunctionFactory {

	public TargetFunction get(TargetFunctionType type) {
		
		if(type == null) {
			return null;
		}
		
		if(type.equals(TargetFunctionType.LINE)) {
			return new Line();
		} else if (type.equals(TargetFunctionType.QUADRATIC)) {
			return new Quadratic();
		}
		return null;
	}
	
}
