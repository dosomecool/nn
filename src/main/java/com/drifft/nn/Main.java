package com.drifft.nn;

import java.io.IOException;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.drifft.nn.gui.Controller;
import com.jfoenix.controls.JFXDecorator;
import com.sun.javafx.application.LauncherImpl;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * DRIFFT - NEURAL NETWORK - USER INTERFACE!
 * 
 */

@SpringBootApplication
@ComponentScan(basePackages="com.drifft.nn")
public class Main extends Application {
	private ConfigurableApplicationContext context;
	private Scene scene = null;
	private JFXDecorator decorator = null;
	private Controller controller = null; 

	@Override 
	public void init() throws IOException {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(Main.class);
		context = builder.run(getParameters().getRaw().toArray(new String[0]));
	}
	
	@Override 
	public void start(Stage stage) {
		this.controller = new Controller(this);
		
		this.decorator = new JFXDecorator(stage, controller.getMainScreen());
		this.decorator.setCustomMaximize(true);
		
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		double width = visualBounds.getWidth();
		double height = visualBounds.getHeight();
		
		this.scene = new Scene(decorator, width, height);
		
		stage.getIcons().add(new Image(getClass().getResource("/com/drifft/nn/gui/media/favicon.png").toExternalForm()));
		stage.setTitle("DRIFFT - NEURAL NETWORK - USER INTERFACE");
		stage.setScene(scene);
		stage.show();
	}
	
	public void reset() {
		this.controller = new Controller(this);
		this.controller.getMainScreen().setTop(null);
		this.decorator.setContent(controller.getMainScreen());
		this.scene.setRoot(this.decorator);
	}
		
	public static void main(String[] args) {
		LauncherImpl.launchApplication(Main.class, args);
	}
	
	@Override
	public void stop() throws Exception {
		context.close();
	}
}
