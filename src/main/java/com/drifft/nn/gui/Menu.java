package com.drifft.nn.gui;

import java.io.IOException;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;

public class Menu extends HBox {

    @FXML
    private HBox rootHBox;

    @FXML
    private JFXButton loadButton;

    @FXML
    private JFXButton saveButton;

    @FXML
    private JFXButton importButton;

    @FXML
    private JFXButton exportButton;

	private Controller controller;
	private String directoryHistory = "";	

	public Menu(Controller controller) {
		this.controller = controller; 

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void initialize() {
		importButton.disableProperty().set(true);
		exportButton.disableProperty().set(true);
	
		loadButton.setOnAction( e -> {
			ProjectLoader projectLoader = new ProjectLoader(this.controller, directoryHistory); 
			projectLoader.loadProject();
			directoryHistory = projectLoader.getDirectory();
		});

		saveButton.setOnAction( e -> {
			ProjectLoader projectLoader = new ProjectLoader(this.controller, directoryHistory); 
			projectLoader.saveProject();
			directoryHistory = projectLoader.getDirectory();
		});
	}
}
