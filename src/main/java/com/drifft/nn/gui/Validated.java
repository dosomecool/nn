package com.drifft.nn.gui;

public interface Validated {
	public void validate();
	
	
//	DoubleValidator.java	added default constructors for validators	2 months ago
//	IntegerValidator.java	added default constructors for validators	2 months ago
//	NumberValidator.java	fixed some findbugs issues	6 months ago
//	RegexValidator.java	added default constructors for validators	2 months ago
//	RequiredFieldValidator.java	added default constructors for validators	2 months ago
//	ValidationFacade.java
}
