package com.drifft.nn.gui;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.jfoenix.controls.JFXButton;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import javafx.scene.shape.SVGPath;

public final class Splasher extends HBox {

    @FXML
    private HBox rootHBox;

    @FXML
    private HBox leftHBox;

    @FXML
    private HBox logoHBox;

    @FXML
    private SVGPath dSvgPath;

    @FXML
    private SVGPath drifftSvgPath;

    @FXML
    private HBox rightHBox;

    @FXML
    private JFXButton dissmissButton;


    private Controller controller;
    
	public Splasher(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Splash.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		Timer timer = new Timer();
		timer.schedule(expiration, 60000l);
		
		dissmissButton.setOnAction( e -> {
			dismiss();
		});
	}	
	
	TimerTask expiration = new TimerTask() {

		@Override
		public void run() {

			Platform.runLater(new Runnable() {

				@Override 
				public void run() {
					dismiss();
				}
			});
		}
	};
	
	private void dismiss() {
		controller.dismissAlert();
	}
}
