package com.drifft.nn.gui;

import java.io.IOException;

import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;

public class SpinnerLabel extends VBox {

    @FXML
    private VBox rootVBox;

    @FXML
    private JFXSpinner spinner;

    @FXML
    private JFXTextField epochErrorTextField;
	
	public SpinnerLabel() {
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Spinner.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void disableTextField() {
		epochErrorTextField.setDisable(true);
	}
	
	@FXML
	public void initialize() {
	}

	// *** Getters / Setters ***
	
	public JFXSpinner getSpinner() {
		return spinner;
	}

	public JFXTextField getEpochErrorTextField() {
		return epochErrorTextField;
	}
}
