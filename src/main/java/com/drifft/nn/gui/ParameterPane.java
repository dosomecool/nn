package com.drifft.nn.gui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;

public class ParameterPane extends HBox {

    @FXML
    private HBox rootHBox;
    
    public ParameterPane() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Parameter.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
