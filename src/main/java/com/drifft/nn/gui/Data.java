package com.drifft.nn.gui;

public class Data {

	private double x;
	private double y;
	private String message;
		
	public Data(double x, double y, String message) {
		this.x = x;
		this.y = y;
		this.message = message;
	}

	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
