package com.drifft.nn.gui;

import com.jfoenix.validation.base.ValidatorBase;

import javafx.beans.DefaultProperty;
import javafx.scene.control.TextInputControl;

@DefaultProperty(value = "icon")
public class RangeValidator extends ValidatorBase {

	private double min = Double.MIN_VALUE;
	private double max = Double.MAX_VALUE;

	public RangeValidator() {
		setMessage("Valid range: [" + min + ", " + max + "]");
	}

	public RangeValidator(final double min, final double max) { 	
		super(new String("Valid range: [" + min + ", " + max + "]"));
		this.min = min;
		this.max = max;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void eval() {
		if (srcControl.get() instanceof TextInputControl) {
			evalTextInputField();
		}
	}

	private void evalTextInputField() {
		TextInputControl textField = (TextInputControl) srcControl.get();
		double value = Double.MIN_VALUE;

		try {
			value = Double.parseDouble(textField.getText());
		} catch (Exception e) {
			// double-validator deals with this
		}

		if(value >= min && value <= max) {
			hasErrors.set(false);
		} else {
			hasErrors.set(true);
		}
	}
}