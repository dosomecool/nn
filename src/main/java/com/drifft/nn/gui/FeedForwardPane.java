package com.drifft.nn.gui;

import java.io.IOException;

import com.drifft.nn.network.Types.CostFunctionType;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.DoubleValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class FeedForwardPane extends VBox implements Validated {

    @FXML
    private Label label;

    @FXML
    private JFXTextField varianceTextField;

    @FXML
    private JFXTextField seedTextField;

    @FXML
    private JFXComboBox<CostFunctionType> costFunctionDropDown;
    
    private Double varianceTextFieldValue;
    private Long seedTextFieldValue;
    private CostFunctionType costFunctionDropDownValue;
    
    private Controller controller;
    
    public FeedForwardPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("FeedForward.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    public void initialize() {
		// Cost function
		for (CostFunctionType type : CostFunctionType.values()) {
			costFunctionDropDown.getItems().add(type);
		}
    	
		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("INPUT IS REQUIRED");
		
		DoubleValidator doubleValidator = new DoubleValidator();
		doubleValidator.setMessage("INPUT IS NOT NUMERIC");
		
		varianceTextField.getValidators().addAll(requiredvalidator, doubleValidator);
		varianceTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {
			
		    if(!newValue) {
		    	varianceTextField.validate();
		    } 
		});

		varianceTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			varianceTextField.setStyle("-fx-effect: null;");
			
			try {
				varianceTextFieldValue = Double.parseDouble(newValue);
			} catch(NumberFormatException ex) {
				// No action necessary
			}
		});	
		
		seedTextField.getValidators().addAll(requiredvalidator, doubleValidator);
		seedTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {
			
		    if(!newValue) {
		    	seedTextField.validate();
		    } 
		});

		seedTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			seedTextField.setStyle("-fx-effect: null;");

			try {
				seedTextFieldValue = Long.parseLong(newValue);
			} catch(NumberFormatException ex) {
				// No action necessary
			}
		});	
		
		costFunctionDropDown.getValidators().add(requiredvalidator);
		costFunctionDropDown.focusedProperty().addListener((observable, oldValue, newValue)->{
			
		    if(!newValue) {
		    	costFunctionDropDown.validate();
		    } 
		});
		
		costFunctionDropDown.valueProperty().addListener((observable, oldValue, newValue) -> {
			costFunctionDropDown.setStyle("-fx-effect: null;");

			if(newValue != null) {
				costFunctionDropDownValue = newValue;
			} 
		});
    }
    
	@Override
	public void validate() {
		
		for(ValidatorBase validator: varianceTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || varianceTextFieldValue == null) {
				controller.submitErroneousField(varianceTextField);
			}
		}
		
		for(ValidatorBase validator: seedTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || seedTextFieldValue == null) {
				controller.submitErroneousField(seedTextField);
			}
		}
		
		for(ValidatorBase validator: costFunctionDropDown.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || costFunctionDropDownValue == null) {
				controller.submitErroneousField(costFunctionDropDown);
			} 
		}
	}
	
	public void setTitle(String title) {
		this.label.setText(title);
	} 
	
	// *** Getters / Setters ***
	

	public JFXTextField getVarianceTextField() {
		return varianceTextField;
	}

	public JFXTextField getSeedTextField() {
		return seedTextField;
	}

	public JFXComboBox<CostFunctionType> getCostFunctionDropDown() {
		return costFunctionDropDown;
	}
	
	public double getVarianceTextFieldValue() {
		return varianceTextFieldValue;
	}

	public long getSeedTextFieldValue() {
		return seedTextFieldValue;
	}

	public CostFunctionType getCostFunctionDropDownValue() {
		return costFunctionDropDownValue;
	}
}
