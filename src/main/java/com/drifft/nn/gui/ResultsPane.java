package com.drifft.nn.gui;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ResultsPane extends VBox {

	@FXML
	private VBox rootVBox;

	@FXML
	private Label title;

	@FXML
	private HBox spinnerHBox;

	private Controller controller;

	private final SpinnerLabel trainingSpinner = new SpinnerLabel();
	private final SpinnerLabel validationSpinner = new SpinnerLabel();
	private final SpinnerLabel predictionSpinner = new SpinnerLabel();

	public ResultsPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Results.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void initialize() {		
		trainingSpinner.getEpochErrorTextField().setPromptText("TRAINING ERROR");
		validationSpinner.getEpochErrorTextField().setPromptText("VALIDATION ERROR");
		predictionSpinner.getEpochErrorTextField().setPromptText("PREDICTION");
		predictionSpinner.disableTextField();
		spinnerHBox.getChildren().add(trainingSpinner);
		spinnerHBox.getChildren().add(validationSpinner);
		spinnerHBox.getChildren().add(predictionSpinner);
	}

	public void setTitle(String title) {
		this.title.setText(title);
	}

	public void onIterationChange(int iteration) {


		int epochs = controller.getNetwork().getBackPropagation().getEpochs(); 
		int rows = controller.getNetwork().getData().getTrainingSequence().size();
		double progress = (double) iteration / (rows * epochs);

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				trainingSpinner.getSpinner().setProgress(progress);
			}
		});
	}

	public void onEpochErrorChange(double error) {
		final String message = getFormattedErrorLabel(error);

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				trainingSpinner.getEpochErrorTextField().setText(message);
			}
		});
	}

	public void onValidationProgressChange(double index) {

		int rows = controller.getNetwork().getData().getValidationSequence().size();
		final double progress = (double) index / rows;
		
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				validationSpinner.getSpinner().setProgress(progress);
			}
		});
	}

	public void onValidationErrorChange(double error) {
		final String message = getFormattedErrorLabel(error);

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				validationSpinner.getEpochErrorTextField().setText(message);
			} 
		});
	}

	public void onPredictionProgressChange(double predictionProgress) {

		int rows = controller.getNetwork().getData().getPredictionSequence().size();
		final double progress = (double) predictionProgress / rows;

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				predictionSpinner.getSpinner().setProgress(progress);
			}
		});

	}

	private String getFormattedErrorLabel(Double error) {
		DecimalFormat decimalFormat = new DecimalFormat("#.###");
		decimalFormat.setRoundingMode(RoundingMode.CEILING);
		String message = error == null ? "NaN" : String.valueOf(controller.getNetwork().getFeedForward().getCostFunction().getCostFunctionType()) 
				+ ": " + String.valueOf(decimalFormat.format(error));
		return message;
	}

	// *** Getters / Setters *** 

	public SpinnerLabel getTrainingSpinner() {
		return trainingSpinner;
	}

	public SpinnerLabel getValidationSpinner() {
		return validationSpinner;
	}

	public SpinnerLabel getPredictionSpinner() {
		return predictionSpinner;
	}
}
