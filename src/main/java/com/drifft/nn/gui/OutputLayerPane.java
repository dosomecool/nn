package com.drifft.nn.gui;

import java.io.IOException;

import com.drifft.nn.network.Types.ActivationType;
import com.drifft.nn.network.Types.DataType;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.IntegerValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class OutputLayerPane extends VBox implements Validated {

	@FXML
	private Label label;

	@FXML
	private JFXTextField nodesTextField;

	@FXML
	private JFXComboBox<ActivationType> activationFunctionDropDown;

	@FXML
	private JFXComboBox<DataType> outputTypeDropDown;
	
	private Integer nodesTextFieldValue;
	private ActivationType activationFunctionDropDownValue;
	
	private Controller controller; 
	
	public OutputLayerPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("OutputLayer.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void initialize() {
		// Activation function
		for (ActivationType type : ActivationType.values()) {
			activationFunctionDropDown.getItems().add(type);
		}
		
		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("INPUT IS REQUIRED");

		IntegerValidator integerValidator = new IntegerValidator();
		integerValidator.setMessage("INPUT IS NOT AN INTEGER");

		// Nodes
		nodesTextField.getValidators().addAll(requiredvalidator, integerValidator);
		nodesTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if(!newValue) {
				nodesTextField.validate();
			} 
		});

		nodesTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, 
					String oldValue, String newValue) {
				nodesTextField.setStyle("-fx-effect: null;");

				try {
					nodesTextFieldValue = Integer.parseInt(newValue);
				} catch(NumberFormatException ex) {
					// No action necessary
				}
			}
		});

		// Normalization
		activationFunctionDropDown.getValidators().addAll(requiredvalidator);
		activationFunctionDropDown.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if(!newValue) {
				activationFunctionDropDown.validate();
			} 
		});

		activationFunctionDropDown.valueProperty().addListener((observable, oldValue, newValue) -> {
			activationFunctionDropDown.setStyle("-fx-effect: null;");

			if(newValue != null) {
				activationFunctionDropDownValue = newValue;
			}
		});
	}
	
	public void setTitle(String title) {
		this.label.setText(title);
	}
	
	@Override
	public void validate() {

		for(ValidatorBase validator: nodesTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || nodesTextFieldValue == null) {
				controller.submitErroneousField(nodesTextField);
			}
		}
		
		for(ValidatorBase validator: activationFunctionDropDown.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || activationFunctionDropDownValue == null) {				
				controller.submitErroneousField(activationFunctionDropDown);
			}
		}
	}
	
	// *** Getters / Setters ***
	
	public JFXTextField getNodesTextField() {
		return nodesTextField;
	}
	
	public JFXComboBox<ActivationType> getActivationFunctionDropDown() {
		return activationFunctionDropDown;
	}

	public JFXComboBox<DataType> getOutputTypeDropDown() {
		return outputTypeDropDown;
	}

	public int getNodesTextFieldValue() {
		return nodesTextFieldValue;
	}

	public ActivationType getActivationFunctionDropDownValue() {
		return activationFunctionDropDownValue;
	}
}
