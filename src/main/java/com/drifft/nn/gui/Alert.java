package com.drifft.nn.gui;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRippler;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

public class Alert extends AnchorPane {

    @FXML
    private AnchorPane rootAnchor;

    @FXML
    private Label alertLabel;

    @FXML
    private JFXRippler alertRippler;

    @FXML
    private HBox buttonContainerHBox;

    @FXML
    private JFXButton alertConfirmButton;

    @FXML
    private JFXButton alertDismissButton;

	private Controller controller;
	private String message;

	private boolean isConfirmButtonShown = false;
	
	Timeline animator = new Timeline(new KeyFrame(Duration.seconds(5), new EventHandler<ActionEvent>() {  

		@Override  
		public void handle(ActionEvent event) {  			
			Runnable release = alertRippler.createManualRipple();
			release.run();
		}  
	})); 

	TimerTask expiration = new TimerTask() {

		@Override
		public void run() {

			Platform.runLater(new Runnable() {

				@Override 
				public void run() {
					dismiss();
				}
			});
		}
	};

	public Alert(Controller controller, String message) {
		this.controller = controller;
		this.message = message;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Alert.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Alert(Controller controller, String message, boolean isConfirmButtonShown) {
		this.controller = controller;
		this.message = message;
		this.isConfirmButtonShown = isConfirmButtonShown; 

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Alert.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		alertConfirmButton.setVisible(false);
		
		alertConfirmButton.setOnAction(e -> {
			confirmButtonAction();
		});
		
		if(isConfirmButtonShown) {
			alertConfirmButton.setVisible(true);
		}
		
		Timer timer = new Timer();
		timer.schedule(expiration, 30000l);

		alertRippler.setControl(alertLabel);
		animator.setCycleCount(Timeline.INDEFINITE);
		animator.play();

		alertDismissButton.setOnAction( e -> {
			dismiss();
		});

		alertLabel.setText(message);		
		this.controller.getMainScreen().setTop(this);
	}

	private void dismiss() {
		animator.stop();
		controller.dismissAlert();
	}

	public void confirmButtonAction() {
		System.out.println("Confirm button action");
	}
}
