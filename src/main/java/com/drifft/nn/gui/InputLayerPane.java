package com.drifft.nn.gui;

import java.io.IOException;

import com.drifft.nn.network.Types.NormalizationType;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.IntegerValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class InputLayerPane extends VBox implements Validated {

	@FXML
	private Label label;

	@FXML
	private JFXTextField nodesTextField;

	@FXML
	private JFXComboBox<NormalizationType> normalizationDropDown;

	private Integer nodesTextFieldValue;
	private NormalizationType normalizationDropDownValue;

	private Controller controller;

	public InputLayerPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("InputLayer.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		// Normalization
		for (NormalizationType type : NormalizationType.values()) {
			normalizationDropDown.getItems().add(type);
		}
		
		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("INPUT IS REQUIRED");

		IntegerValidator integerValidator = new IntegerValidator();
		integerValidator.setMessage("INPUT IS NOT AN INTEGER");

		// Nodes
		nodesTextField.getValidators().addAll(requiredvalidator, integerValidator);
		nodesTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {
			
			if(!newValue) {
				nodesTextField.validate();
			} 
		});

		nodesTextField.textProperty().addListener(new ChangeListener<String>() {
			
			@Override
			public void changed(ObservableValue<? extends String> observable, 
					String oldValue, String newValue) {
				nodesTextField.setStyle("-fx-effect: null");

				try {
					nodesTextFieldValue = Integer.parseInt(newValue);
				} catch(NumberFormatException ex) {
					// No action necessary
				}
			}
		});

		// Normalization
		normalizationDropDown.getValidators().addAll(requiredvalidator);
		normalizationDropDown.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if(!newValue) {
				normalizationDropDown.validate();
			} 
		});

		normalizationDropDown.valueProperty().addListener((observable, oldValue, newValue) -> {
			normalizationDropDown.setStyle("-fx-effect: null");
			
			if(newValue != null) {
				normalizationDropDownValue = newValue;
			}
		});
	}
	
	@Override
	public void validate() {

		for(ValidatorBase validator: nodesTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || nodesTextFieldValue == null) {
				controller.submitErroneousField(nodesTextField);
			}
		}
		
		for(ValidatorBase validator: normalizationDropDown.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || normalizationDropDownValue == null) {
				controller.submitErroneousField(normalizationDropDown);
			}
		}
		
//		for(ValidatorBase validator: inputTypeDropDown.getValidators()) {
//			
//			if(validator.hasErrorsProperty().getValue() || inputTypeDropDown == null) {
//				controller.submitErroneousField(inputTypeDropDown);
//			} 
//		}
	}
	
	public void setTitle(String title) {
		this.label.setText(title);
	}
	
	// *** Getters / Setters ***
	
	public JFXTextField getNodesTextField() {
		return nodesTextField;
	}
	
	public JFXComboBox<NormalizationType> getNormalizationDropDown() {
		return normalizationDropDown;
	}

	public int getNodesTextFieldValue() {
		return nodesTextFieldValue;
	}

	public NormalizationType getNormalizationDropDownValue() {
		return normalizationDropDownValue;
	}
}
