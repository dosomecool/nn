package com.drifft.nn.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class HiddenLayersPane extends VBox {

    @FXML
    private VBox rootVBox;
	
    @FXML
    private Label label;

    @FXML
    private VBox hiddenLayersContainer;

    @FXML
    private JFXButton addHiddenLayerButton;
    
    private Controller controller;
 
	public HiddenLayersPane(Controller controller) {
		this.controller = controller;
	
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("HiddenLayers.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void initialize() {
		addHiddenLayer();
		addHiddenLayer();
		addHiddenLayer();
		
		addHiddenLayerButton.setOnAction( e -> {
			addHiddenLayer();
		});
	}
	
	public void addHiddenLayer() {		
		
		if(hiddenLayersContainer.getChildren().size() <= 99) {
			HiddenLayerPane hiddenLayer = new HiddenLayerPane(controller, this); 
			int index = hiddenLayersContainer.getChildren().size(); 
			hiddenLayer.setIndex(index);
			hiddenLayersContainer.getChildren().add(hiddenLayer);
		}
	}
	
	public void removeHiddenLayer(int index) {
	
		if(hiddenLayersContainer.getChildren().size() > 1) {
			hiddenLayersContainer.getChildren().remove(index);
			for(int i=0; i<hiddenLayersContainer.getChildren().size(); i++) {
				((HiddenLayerPane) hiddenLayersContainer.getChildren().get(i)).setIndex(i);
			}	
		}
	}
		
	public List<HiddenLayerPane> getHiddenLayerList() {
		List<HiddenLayerPane> hiddenLayerList = new ArrayList<>();
		
		for(Node pane : hiddenLayersContainer.getChildren()) {			
			hiddenLayerList.add((HiddenLayerPane) pane);
		}
		return hiddenLayerList;
	}

	public void setTitle(String title) {
		this.label.setText(title);
	}
	
	// *** Getter / Setter *** 
	
	
	
	public VBox getHiddenLayersContainer() {
		return hiddenLayersContainer;
	}

	public JFXButton getAddHiddenLayerButton() {
		return addHiddenLayerButton;
	}
}
