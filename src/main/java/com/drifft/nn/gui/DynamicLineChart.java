package com.drifft.nn.gui;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

// https://stackoverflow.com/questions/44536365/how-can-i-display-value-while-hovering-on-the-line-chart-points-in-javafx

public class DynamicLineChart {

	// Size
	private final static double WIDTH = 480;
	private final static double HEIGHT = 320;
	private final static int MAX_POINTS = 2500;
	private final static int XAXIS_TICK_UNITS = 250;
	
	// Defining the axes
	final NumberAxis xAxis = new NumberAxis();
	final NumberAxis yAxis = new NumberAxis();
	
	private IntegerProperty totalPointsProperty = new SimpleIntegerProperty(MAX_POINTS);
	private IntegerProperty xAxisTickUnitsProperty = new SimpleIntegerProperty(XAXIS_TICK_UNITS);
	
	// Chart data
	private Series<Double, Double> series;
	private ObservableList<XYChart.Data<Double, Double>> observableDataList;

	// Line chart	
	private ObservableList<XYChart.Series<Double, Double>> lineChartData = FXCollections.observableArrayList();
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private final LineChart lineChart = new LineChart(xAxis, yAxis, lineChartData);

	
	// Data queue
//	private BlockingQueue<Data> queue;
		
	public DynamicLineChart(final String xAxisLabel, final String yAxisLabel) {
	
		// x-axis
		xAxis.setLabel(xAxisLabel);
		xAxis.setMinorTickVisible(false);
		xAxis.setAutoRanging(false);
		xAxis.setLowerBound(0);
		xAxis.setUpperBound(this.totalPointsProperty.get());
		xAxis.setTickUnit(this.xAxisTickUnitsProperty.get());

		// y-axis
		yAxis.setLabel(yAxisLabel);		
		yAxis.setMinorTickVisible(false);
		yAxis.setAutoRanging(true);

		// Set chart properties		
		lineChart.setAnimated(false);
		lineChart.setLegendVisible(false);
		lineChart.setCreateSymbols(false);

		lineChart.setMinSize(WIDTH, HEIGHT);
		lineChart.setMaxSize(WIDTH, HEIGHT);
		
		lineChart.setCache(true);

		// Set series
		observableDataList = FXCollections.observableArrayList();
		series = new LineChart.Series<Double, Double>(yAxisLabel + "-SERIES", observableDataList);

		
		lineChartData.add(series);
	}
	
	public void updateMovingWindowChart(final double x, final double y) {
		series.getData().add(new XYChart.Data<Double, Double>(x, y));
		
		if(series.getData().size() > MAX_POINTS) {
			series.getData().remove(0);
			double lowerBound = series.getData().get(0).getXValue();
			double upperbound = x;
			final double tickUnits  = ( (upperbound - lowerBound) / 10 ); 
			xAxis.setLowerBound(lowerBound);
			xAxis.setUpperBound(upperbound);
			xAxis.setTickUnit(tickUnits);
		}
	}
	
	public void updateChart(final double x, final double y) {
		series.getData().add(new XYChart.Data<Double, Double>(x, y));
	}

	// *** Getters / Setters ***
		
	public IntegerProperty getTotalPointsProperty() {
		return totalPointsProperty;
	}

	public IntegerProperty getxAxisTickUnitsProperty() {
		return xAxisTickUnitsProperty;
	}
	
	@SuppressWarnings("rawtypes")
	public LineChart getChart() {
		return lineChart;
	}
}
