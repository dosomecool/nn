package com.drifft.nn.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.nn.network.Types.NetworkState;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

// Source: https://www.techyourchance.com/thread-safe-observer-design-pattern-in-java/

public class ChartPane extends VBox {

	@FXML
	private Label label;

	@FXML
	private VBox chartContainer;

	private Controller controller;

	private IntegerProperty iterations = new SimpleIntegerProperty();
	private double batchError;

	// Charts
	private DynamicLineChart errorChart;
	private DynamicScatterChart validationChart;
	private DynamicScatterChart predictionChart;
	
	public ChartPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Chart.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void initialize() {
		errorChart = new DynamicLineChart("ITERATIONS", "BATCH ERROR");
		validationChart = new DynamicScatterChart("validation", "INDEX", "VALIDATION OUTPUT");
		predictionChart = new DynamicScatterChart("prediction", "INDEX", "PREDICTION OUTPUT");

		chartContainer.getChildren().add(errorChart.getChart());	
		chartContainer.getChildren().add(validationChart.getChart());
		chartContainer.getChildren().add(predictionChart.getChart());
	}

	private Timeline lineChartUpdater = new Timeline(new KeyFrame(Duration.seconds(0.001), new EventHandler<ActionEvent>() {  

		@Override  
		public void handle(ActionEvent event) {  

			Platform.runLater(new Runnable() {

				@Override
				public void run() {					
					errorChart.updateMovingWindowChart(iterations.get(), batchError);
				}
			});
		}  
	}));

	public void registerState(NetworkState state) {

		if(state == null) {
			return;
		}

		if(state == NetworkState.NETWORK_INITIALIZED) {

		}
	}

	public void onStateChange(NetworkState state) {

		if(state == null) {
			return;
		}

		switch(state) {

		case TRAINING_STARTED:
			swap(errorChart.getChart());
			lineChartUpdater.setCycleCount(Timeline.INDEFINITE);
			lineChartUpdater.play();
			break;
		case TRAINING_SUCCEEDED:
			lineChartUpdater.stop();
			break;
		case TRAINING_STOPPED:
			lineChartUpdater.stop();
			break;
		case TRAINING_FAILED: 
			lineChartUpdater.stop();
			break;

		case VALIDATION_STARTED:
			swap(validationChart.getChart());
			break;
		case VALIDATION_SUCCEEDED:
			break;
		case VALIDATION_FAILED:
			break;

		case PREDICTION_STARTED:
			swap(predictionChart.getChart());
			break;
		case PREDICTION_SUCCEEDED:
			break;
		case PREDICTION_FAILED:
			break;

		default:
			break;
		}		
	}

	public void onIterationChange(int iteration) {
		iterations.set(iteration);
	}

	public void onBatchErrorChange(double error) {
		this.batchError = error;
	}

	public void registerValidationOutput() {
		
		for(int index : controller.getNetwork().getData().getValidationSequence()) {

			double x = index;
			List<Double> y = controller.getNetwork().getData().getOutputRow(index);
			Platform.runLater(new Runnable() {

				@Override
				public void run() {
					validationChart.updateChart(0, x, y);
				}
			});
		}
	}

	public void onValidationOutputChange(int index, List<Double> validationOutputList) {

		double x = index;
		List<Double> ys = new ArrayList<>();

		for(Double d : validationOutputList) {
			ys.add(d);
		}		

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				validationChart.updateChart(1, x, ys);
			}
		});
	}
	
	public void onPredictionOutputChange(int index, List<Double> predictionOutputList) {

		double x = index;
		List<Double> ys = new ArrayList<>();

		for(Double d : predictionOutputList) {
			ys.add(d);
		}

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				predictionChart.updateChart(1, x, ys);
			}
		});
	}

	public void swap(Node node) {
		final Node targetNode = node;
		FadeTransition fadeOut = new FadeTransition(Duration.millis(500), targetNode);
		FadeTransition fadein = new FadeTransition(Duration.millis(200), targetNode);

		fadeOut.setFromValue(1.0);
		fadeOut.setToValue(0.20);
		fadeOut.setCycleCount(1);
		fadeOut.setOnFinished( e -> {
			fadein.play();
			chartContainer.getChildren().remove(targetNode);
			chartContainer.getChildren().add(0, targetNode);		
		});
		fadein.setFromValue(0.20);
		fadein.setToValue(1.0);
		fadein.setCycleCount(1);
		fadeOut.play();
	}

	public void setTitle(String title) {
		this.label.setText(title);		
	}

	public void clearChart(String series) {
		
		switch(series) {
		
		case "validate" : validationChart.clearData(1);
			break;
			
		case "predict" : predictionChart.clearData(1);
			break;
		}
	}

	// *** Getters / Setters ***
}
