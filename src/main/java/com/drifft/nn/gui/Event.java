package com.drifft.nn.gui;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

public class Event {
	
	private String name;
	private ObservableList<XYChart.Series<Number, Number>> series;
	
	private boolean isActive = true;


	public Event(String name, ObservableList<XYChart.Series<Number, Number>> series) {
		this.name = name;  
		this.series = series;
	}
	
	public ObservableList<XYChart.Series<Number, Number>> getSeries() {
		return series;
	}
	
	public String getName() { 
		return name; 
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}