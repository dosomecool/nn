package com.drifft.nn.gui;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import com.drifft.nn.gui.Project.HiddenLayerValues;
import com.drifft.nn.utilities.JsonUtility;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ProjectLoader {

	// Directory history
	private String directory = "";
	private Controller controller;

	public ProjectLoader(Controller controller) {
		this.controller = controller;
	}

	public ProjectLoader(Controller controller, String directory) {
		this.controller = controller;
		this.directory = directory;
	}

	public void loadProject() {
		FileChooser fileChooser = new FileChooser();
		configureFileChooser("SELECT FILE", fileChooser);
		Stage stage = (Stage) controller.getControlsPane().getScene().getWindow();
		File file = fileChooser.showOpenDialog(stage);

		Project project = null; 

		if (file != null) {
			directory = Paths.get(file.toString()).getParent().toString();

			try {
				project = (Project) JsonUtility.readJsonToObject(file.toString(), Project.class);
			} catch (IOException e) {
				new Alert(this.controller, "The project contains unreconginzed fields.");
				//				e.printStackTrace();
			}

		} 

		if(project != null) {

			// File chooser		
			controller.getTrainingDataLoaderPane().setFile(project.getTrainingDataFile());

			// Data loader
			controller.getDataLoaderPane().getTrainingPercentTextField().setText(project.getTrainingPercent().toString());
			controller.getDataLoaderPane().getSeedTextField().setText(project.getDataLoaderSeed().toString());
			controller.getDataLoaderPane().getShuffleDropDown().setValue(project.getShuffleType());;

			// Feedforward
			controller.getFeedForwardPane().getVarianceTextField().setText(project.getVariance().toString());
			controller.getFeedForwardPane().getSeedTextField().setText(project.getfeedForwardSeed().toString());
			controller.getFeedForwardPane().getCostFunctionDropDown().setValue(project.getCostFunctionType());

			// Input layer
			controller.getInputLayerPane().getNodesTextField().setText(project.getInputNodes().toString());
			controller.getInputLayerPane().getNormalizationDropDown().setValue(project.getNormalizationType());

			// Hidden layers
			controller.getHiddenLayersPane().getHiddenLayersContainer().getChildren().clear();

			for(HiddenLayerValues hiddenLayerValues : project.getHiddenLayers()) {
				HiddenLayerPane hiddenLayer = new HiddenLayerPane(controller, controller.getHiddenLayersPane());
				hiddenLayer.setIndex(hiddenLayerValues.getIndex());
				hiddenLayer.getNodesTextField().setText(hiddenLayerValues.getHiddenNodes().toString());
				hiddenLayer.getActivationFunctionDropDown().setValue(hiddenLayerValues.getHiddenLayerActivationFunction());
				controller.getHiddenLayersPane().getHiddenLayersContainer().getChildren().add(hiddenLayer);
			}

			// Output layer
			controller.getOutputLayerPane().getNodesTextField().setText(project.getOutputNodes().toString());
			controller.getOutputLayerPane().getActivationFunctionDropDown().setValue(project.getOutputActivationFunction());

			// Backpropagation
			controller.getBackpropagationPane().getBatchTextField().setText(project.getBatch().toString());
			controller.getBackpropagationPane().getEpochsTextField().setText(project.getEpochs().toString());
			controller.getBackpropagationPane().getThreadsTextField().setText(project.getThreads().toString());
			controller.getBackpropagationPane().getOptimizerDropDown().setValue(project.getOptimizationType());

		}
	}

	public void saveProject() {
		Project project = new Project();

		File file = null; 
		
		try {
			
		// File chooser		
		FileChooserPane trainingDataLoaderPane = controller.getTrainingDataLoaderPane();
		project.setTrainingDataFile(trainingDataLoaderPane.getFile());

		// Data loader
		DataLoaderPane dataLoaderPane = controller.getDataLoaderPane();
		project.setTrainingPercent(dataLoaderPane.getTrainingPercentTextFieldValue());
		project.setDataLoaderSeed(dataLoaderPane.getSeedTextFieldValue());
		project.setShuffleType(dataLoaderPane.getShuffleDropDown().getValue());

		// Feedforward
		FeedForwardPane feedForwardPane = controller.getFeedForwardPane();
		project.setVariance(feedForwardPane.getVarianceTextFieldValue());
		project.setfeedForwardSeed(feedForwardPane.getSeedTextFieldValue());
		project.setCostFunctionType(feedForwardPane.getCostFunctionDropDown().getValue());

		// Input layer		
		InputLayerPane inputLayerPane = controller.getInputLayerPane();
		project.setInputNodes(inputLayerPane.getNodesTextFieldValue());
		project.setNormalizationType(inputLayerPane.getNormalizationDropDown().getValue());

		// Hidden layers
		HiddenLayersPane hiddenLayersPane = controller.getHiddenLayersPane();

		for(HiddenLayerPane hiddenLayerPane : hiddenLayersPane.getHiddenLayerList()) {
			HiddenLayerValues hiddenLayer = new HiddenLayerValues();
			hiddenLayer.setIndex(hiddenLayerPane.getIndex());
			hiddenLayer.setHiddenNodes(hiddenLayerPane.getNodesTextFieldValue());
			hiddenLayer.setHiddenLayerActivationFunction(hiddenLayerPane.getActivationFunctionDropDown().getValue());
			project.getHiddenLayers().add(hiddenLayer);
		} 

		// Output layer		
		OutputLayerPane outputLayerPane = controller.getOutputLayerPane();
		project.setOutputNodes(outputLayerPane.getNodesTextFieldValue());
		project.setOutputActivationFunction(outputLayerPane.getActivationFunctionDropDown().getValue());

		// Backpropagation
		BackpropagationPane backpropagationPane = controller.getBackpropagationPane();
		project.setBatch(backpropagationPane.getBatchTextFieldValue());
		project.setEpochs(backpropagationPane.getEpochsTextFieldValue());
		project.setThreads(backpropagationPane.getThreadsTextFieldValue());
		project.setOptimizationType(backpropagationPane.getOptimizerDropDown().getValue());

		FileChooser fileChooser = new FileChooser();
		configureFileChooser("SAVE FILE", fileChooser);
		Stage stage = (Stage) controller.getControlsPane().getScene().getWindow();
		file = fileChooser.showSaveDialog(stage);

		} catch (NullPointerException ex) {
			new Alert(controller, "The training input must be fully specified!");
			ex.printStackTrace();
		}
		
		if (file != null) {        	
			
			try {
				JsonUtility.writeObjectToJson(file.toString(), project);
			} catch (IOException ex) {
				new Alert(controller, "There was a problem saving the project to file!");
				ex.printStackTrace();
			}
		}	
	}
	
	//	/**
	//	 * 
	//	 */
	//	private static final long serialVersionUID = 1L;
	//
	//	EventType<Loadproject> testEventType = new EventType<Loadproject>("load-previous-values");
	//
	//	
	//	public Loadproject(EventType<Loadproject> eventType) {
	//		super(eventType);
	//	}
	//
	//	
	//
	//	EventHandler<Loadproject> filter = new EventHandler<Loadproject>() {
	//
	//		@Override
	//		public void handle(Loadproject event) {
	//			System.out.println("Filtering out event " + event.getEventType());
	//			event.consume();
	//		}
	//	};

	private void configureFileChooser(String title, final FileChooser fileChooser) {
		fileChooser.setTitle(title);
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home"))); 
		if(!directory.equals("")) {
			fileChooser.setInitialDirectory(new File(directory)); 
		}        
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.json"));
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}
}
