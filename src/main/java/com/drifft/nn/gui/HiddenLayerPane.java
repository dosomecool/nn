package com.drifft.nn.gui;

import java.io.IOException;
import java.util.UUID;

import com.drifft.nn.network.Types.ActivationType;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.IntegerValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class HiddenLayerPane extends HBox implements Validated {
	
    @FXML
    private HBox rootHBox;

    @FXML
    private Label indexLabel;

    @FXML
    private JFXTextField nodesTextField;

    @FXML
    private JFXComboBox<ActivationType> activationFunctionDropDown;

    @FXML
    private JFXButton removeButton;

    private UUID uuid = UUID.randomUUID();
    private IntegerProperty indexProperty = new SimpleIntegerProperty();
    
	private Integer nodesTextFieldValue;
	private ActivationType activationFunctionDropDownValue;

	private Controller controller;
	private HiddenLayersPane parent;

	public HiddenLayerPane(Controller controller, HiddenLayersPane parent) {
		this.parent = parent;
		this.controller = controller;
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("HiddenLayer.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		// Activation Function
		for (ActivationType type : ActivationType.values()) {
			activationFunctionDropDown.getItems().add(type);
		}
		
		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("INPUT IS REQUIRED");
		
		IntegerValidator integerValidator = new IntegerValidator();
		integerValidator.setMessage("INPUT IS NOT AN INTEGER");
			
		// Nodes
		nodesTextField.getValidators().addAll(requiredvalidator, integerValidator);
		nodesTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {
			
		    if(!newValue) {
		    	nodesTextField.validate();
		    } 
		});
		
		nodesTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, 
					String oldValue, String newValue) {
				nodesTextField.setStyle("-fx-effect: null;");
					
				try {
					nodesTextFieldValue = Integer.parseInt(newValue);
				} catch(NumberFormatException ex) {
					// No action necessary
				}
			}
		});
				
		activationFunctionDropDown.getValidators().addAll(requiredvalidator);
		activationFunctionDropDown.focusedProperty().addListener((observable, oldValue, newValue) -> {
			
		    if(!newValue) {
		    	activationFunctionDropDown.validate();
		    } 
		});

		activationFunctionDropDown.valueProperty().addListener(new ChangeListener<ActivationType>() {

			@Override
			public void changed(ObservableValue<? extends ActivationType> observable, 
					ActivationType oldActivation, ActivationType newActivation) {
				activationFunctionDropDown.setStyle("-fx-effect: null;");

				if(newActivation != null) {
					activationFunctionDropDownValue = newActivation;
				}
			}    
		});
		
		// Remove button
		removeButton.setOnAction( e -> {
			parent.removeHiddenLayer(indexProperty.getValue());
		});
		indexLabel.textProperty().bind(indexProperty.asString());
	}
	
	public UUID getUuid() {
		return uuid;
	}
	
	public IntegerProperty getIndexProperty() {
		return indexProperty;
	}

	public void validate() {
		
		for(ValidatorBase validator: nodesTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || nodesTextFieldValue == null) {
				controller.submitErroneousField(nodesTextField);
			}
		}
		
		for(ValidatorBase validator: activationFunctionDropDown.getValidators()) {
		
			if(validator.hasErrorsProperty().getValue() || activationFunctionDropDownValue == null) {
				controller.submitErroneousField(activationFunctionDropDown);
			}
		}
	}
	
	// *** Getters / Setters ***
	
	public int getNodesTextFieldValue() {
		return nodesTextFieldValue;
	}
	
	public ActivationType getActivationFunctionDropDownValue() {
		return activationFunctionDropDownValue;
	}
	
	public int getIndex() {
		return indexProperty.getValue();
	}

	public void setIndex(int value) {
		this.indexProperty.set(value);
	}

	public JFXTextField getNodesTextField() {
		return nodesTextField;
	}

	public JFXComboBox<ActivationType> getActivationFunctionDropDown() {
		return activationFunctionDropDown;
	}

	public JFXButton getRemoveButton() {
		return removeButton;
	}
}
