package com.drifft.nn.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.ift.CellProcessor;

import com.drifft.nn.Main;
import com.drifft.nn.backpropagation.Backpropagation;
import com.drifft.nn.backpropagation.MultiThreadBackpropagation;
import com.drifft.nn.backpropagation.SingleThreadBackpropagation;
import com.drifft.nn.network.DataLoader;
import com.drifft.nn.network.FeedForward;
import com.drifft.nn.network.Network;
import com.drifft.nn.network.NetworkService;
import com.drifft.nn.network.Types.ActivationType;
import com.drifft.nn.network.Types.NetworkState;
import com.drifft.nn.normalization.MeanNormalization;
import com.drifft.nn.utilities.CsvUtility;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;

public class Controller implements NetworkService {

	// Main class
	private Main main; 

	// Neural Network 
	private Network network;

	// GUI components
	private Splasher splasher; 
	private RootPane mainScreen;
	private Menu menu;
	private ControlsPane controlsPane;
	private ResultsPane resultsPane;
	private ChartPane chartPane;
	private FileChooserPane trainingDataLoaderPane;
	private FileChooserPane predictionDataLoaderPane;
	private DataLoaderPane dataLoaderPane;
	private FeedForwardPane feedForwardPane;
	private InputLayerPane inputLayerPane;
	private HiddenLayersPane hiddenLayersPane;
	private OutputLayerPane outputLayerPane;
	private BackpropagationPane backpropagationPane;

	private final List<Node> errouneousNodeList = new ArrayList<>();

	@SuppressWarnings("static-access")
	public Controller(Main main) {
		this.main = main;

		this.mainScreen = new RootPane(this);

		this.splasher = new Splasher(this); 
		this.mainScreen.setTop(splasher);

		this.menu = new Menu(this);
		this.mainScreen.getCenterVBox().getChildren().add(0, menu);

		this.controlsPane = new ControlsPane(this);
		Node firstControlsNode = this.controlsPane.getChildren().get(0);
		this.controlsPane.setMargin(firstControlsNode, new Insets(20, 0, 0, 0));
		this.mainScreen.getRightVBox().getChildren().add(controlsPane);

		this.resultsPane = new ResultsPane(this);
		this.mainScreen.getRightVBox().getChildren().add(resultsPane);

		this.chartPane = new ChartPane(this);
		this.mainScreen.getRightVBox().getChildren().add(chartPane);

		this.trainingDataLoaderPane = new FileChooserPane(this);
		Node firstTrainingDataLoaderNode = this.trainingDataLoaderPane.getChildren().get(0);
		this.trainingDataLoaderPane.setMargin(firstTrainingDataLoaderNode, new Insets(20, 0, 0, 0));
		this.trainingDataLoaderPane.setTitle("TRAINING DATA");
		this.mainScreen.getLeftVBox().getChildren().add(trainingDataLoaderPane);

		this.predictionDataLoaderPane = new FileChooserPane(this);
		this.predictionDataLoaderPane.setTitle("PREDICTION DATA");
		this.mainScreen.getLeftVBox().getChildren().add(predictionDataLoaderPane);

		this.dataLoaderPane = new DataLoaderPane(this);
		this.dataLoaderPane.setTitle("DATA LOADER");
		this.mainScreen.getLeftVBox().getChildren().add(dataLoaderPane);

		this.feedForwardPane = new FeedForwardPane(this);
		this.feedForwardPane.setTitle("FEED FORWARD");
		this.mainScreen.getLeftVBox().getChildren().add(feedForwardPane);

		this.inputLayerPane = new InputLayerPane(this); 
		this.inputLayerPane.setTitle("INPUT LAYER");
		this.mainScreen.getLeftVBox().getChildren().add(inputLayerPane);

		this.hiddenLayersPane = new HiddenLayersPane(this);
		this.hiddenLayersPane.setTitle("HIDDEN LAYERS");
		this.mainScreen.getLeftVBox().getChildren().add(hiddenLayersPane);

		this.outputLayerPane = new OutputLayerPane(this);
		this.outputLayerPane.setTitle("OUTPUT LAYER");
		this.mainScreen.getLeftVBox().getChildren().add(outputLayerPane);

		this.backpropagationPane = new BackpropagationPane(this);
		this.backpropagationPane.setTitle("BACKPROPAGATION");
		this.mainScreen.getLeftVBox().getChildren().add(backpropagationPane);
	}

	// Create user input
	public boolean createNetwork() {		
		validateAllTrainingRequiredComponents();

		if(errouneousNodeList.size() == 0) {

			try {
				// Read training data from csv
				String filename = trainingDataLoaderPane.getPath();
				String[] headers = null;
				CellProcessor[] processors;
				List<Map<String, Object>> rawData = null;

				try {
					headers    = CsvUtility.getHeaders(filename);
					processors = CsvUtility.getProcessors(headers.length);
					rawData    = CsvUtility.readCsvWithHeaders(filename, processors);

				} catch (IOException e) {
					throw new IllegalStateException(e.getMessage());
					// need alert here maybe
				} 

				// Specify inputs and outputs from the csv
				int inputs  = inputLayerPane.getNodesTextFieldValue();
				int outputs = outputLayerPane.getNodesTextFieldValue();

				// Check input + output <= headers
				if( (inputs + outputs) > headers.length ) {
					throw new IllegalStateException("input + output > data columns");
				}

				trainingDataLoaderPane.setRows(rawData.size());

				// Normalize data
				List<MeanNormalization> normalizedColumns = new ArrayList<>();
				List<Map<String, Object>> normalizedData = new ArrayList<>(); 

				for(int i=0; i<rawData.size(); i++) {
					Map<String, Object> row = new HashMap<>();	
					normalizedData.add(row);  
				}

				for(int i=0; i<headers.length; i++) {
					normalizedColumns.add(new MeanNormalization(headers[i])); 
					normalizedColumns.get(i).normalizeData(rawData, normalizedData); 
				}

				System.out.println("data size: " + rawData.size());
				System.out.println("norm size: " + normalizedData.size());

				try {
					CsvUtility.writeWithHeaders(filename.replace(".csv", "Normalized.csv"), headers, processors, normalizedData);
				} catch (IOException e) {
					throw new IllegalStateException(e.getMessage());
					// need alert here maybe
				}

				// Data utility provides important methods used by the network
				DataLoader data = new DataLoader.DataLoaderBuilder(inputs, outputs, headers, normalizedData)
						.setTrainPercent(dataLoaderPane.getTrainingPercentTextFieldValue())
						.setSeed(dataLoaderPane.getSeedTextFieldValue())
						.setShuffle(dataLoaderPane.isShuffled())
						.build();

				// Set the feed forward options
				FeedForward feedForward = new FeedForward.FeedForwardBuilder()
						.setSeed(feedForwardPane.getSeedTextFieldValue())
						.setVariance(feedForwardPane.getVarianceTextFieldValue())
						.setCostFunction(feedForwardPane.getCostFunctionDropDownValue())
						.build();

				// Create the feed-forward network
				feedForward.createInputLayer(inputs);

				for(int i=0; i<hiddenLayersPane.getHiddenLayerList().size(); i++) {
					int hiddenNodes = hiddenLayersPane.getHiddenLayerList().get(i).getNodesTextFieldValue();
					ActivationType activationType = hiddenLayersPane.getHiddenLayerList().get(i).getActivationFunctionDropDownValue();					
					feedForward.createHiddenLayer(hiddenNodes, activationType);
				}

				feedForward.createOutputLayer(outputs, outputLayerPane.getActivationFunctionDropDownValue());
				feedForward.createConnections();

				Backpropagation backpropagation = null;
				int threads = backpropagationPane.getThreadsTextFieldValue();

				if(threads == 1) {

					backpropagation = new SingleThreadBackpropagation.BackpropagationBuilder(feedForward)
							.setBatch(backpropagationPane.getBatchTextFieldValue())
							.setEpochs(backpropagationPane.getEpochsTextFieldValue())
							.setOptimizer(backpropagationPane.getOptimizerDropDownValue(), backpropagationPane.getParameters())
							.build();

				} else if (threads > 1) {	

					backpropagation = new MultiThreadBackpropagation.BackpropagationBuilder(feedForward)
							.setBatch(backpropagationPane.getBatchTextFieldValue())
							.setEpochs(backpropagationPane.getEpochsTextFieldValue())
							.setThreads(threads) 
							.setOptimizer(backpropagationPane.getOptimizerDropDownValue(), backpropagationPane.getParameters())
							.build();
				}	
				this.network = new Network(data, feedForward, backpropagation);
				registerObserver();

			} catch(IllegalStateException ex) { // TODO: Create custom exceptions
				new Alert(this, "Number of inputs and outputs exceeds the number of data columns!");
				highlightNode(inputLayerPane.getNodesTextField()); 
				highlightNode(outputLayerPane.getNodesTextField()); 
				return false;
			}

		} else {
			new Alert(this, "Please check the input fields below!");

			for(Node node : errouneousNodeList) {
				highlightNode(node);
			}
			return false;
		}
		return true;
	}

	public boolean updateNetwork() {

		validateAllTrainingRequiredComponents();

		if(errouneousNodeList.size() == 0) {

			// The orginal data does not get updated
			int inputs = this.network.getData().getInputs();
			int outputs = this.network.getData().getOutputs(); 
			String[] headers = this.network.getData().getHeaders(); 
			List<Map<String, Object>> rawData = this.network.getData().getTrainingData();

			// Data utility provides important methods used by the network
			DataLoader data = new DataLoader.DataLoaderBuilder(inputs, outputs, headers, rawData)
					.setTrainPercent(dataLoaderPane.getTrainingPercentTextFieldValue())
					.setSeed(dataLoaderPane.getSeedTextFieldValue())
					.setShuffle(dataLoaderPane.isShuffled())
					.build();

			// Set the feed forward options
			FeedForward feedForward = new FeedForward.FeedForwardBuilder()
					.setSeed(feedForwardPane.getSeedTextFieldValue())
					.setVariance(feedForwardPane.getVarianceTextFieldValue())
					.setCostFunction(feedForwardPane.getCostFunctionDropDownValue())
					.build();

			// Create the feed-forward network
			feedForward.createInputLayer(inputs);

			for(int i=0; i<hiddenLayersPane.getHiddenLayerList().size(); i++) {
				int hiddenNodes = hiddenLayersPane.getHiddenLayerList().get(i).getNodesTextFieldValue();
				ActivationType activationType = hiddenLayersPane.getHiddenLayerList().get(i).getActivationFunctionDropDownValue();					
				feedForward.createHiddenLayer(hiddenNodes, activationType);
			}

			feedForward.createOutputLayer(outputs, outputLayerPane.getActivationFunctionDropDownValue());
			feedForward.createConnections();

			// Transfer the weights from the old feedforward
			feedForward.setWeights(this.network.getFeedForward().getWeights(false));

			SingleThreadBackpropagation backpropagation = null;

			if(backpropagationPane.getThreadsTextFieldValue() == 1) {

				// Set the backpropagation options
				backpropagation = new SingleThreadBackpropagation.BackpropagationBuilder(feedForward)
						.setBatch(backpropagationPane.getBatchTextFieldValue())
						.setEpochs(backpropagationPane.getEpochsTextFieldValue())
						.setOptimizer(backpropagationPane.getOptimizerDropDownValue(), backpropagationPane.getParameters())
						.build();
			} else {				
				// Multithread backpropagation
			}

			// Create a new network with old weights and new parameters
			this.network = new Network(data, feedForward, backpropagation);				
			registerObserver();

		} else {
			new Alert(this, "Please check the input fields below!");

			for(Node node : errouneousNodeList) {
				highlightNode(node);
			}
			return false;
		}
		return false;
	}

	public boolean createPredictionData() {
		validateAllPredictionRequiredComponents();

		if(errouneousNodeList.size() == 0) {

			try {
				// Read training data from csv
				String filename = predictionDataLoaderPane.getPath();
				String[] headers = null;
				CellProcessor[] processors;
				List<Map<String, Object>> rawData = null;

				try {
					headers    = CsvUtility.getHeaders(filename);

					// check the headers length is the same
					if( this.network.getData().getHeaders().length != headers.length ) {
						throw new IllegalStateException("The number of columns in the prediction set do not match the number of columns in the training set!");
					}

					int inputs = this.network.getData().getInputs();
					int outputs = this.network.getData().getOutputs();
					processors = CsvUtility.getProcessors(inputs, outputs);
					rawData    = CsvUtility.readCsvWithHeaders(filename, processors);

				} catch (IOException e) {
					throw new IllegalStateException(e.getMessage());
					// need alert here maybe
				}

				predictionDataLoaderPane.setRows(rawData.size());	
				this.network.getData().setPredictionData(rawData);

			} catch(IllegalStateException ex) { 
				new Alert(this, "The number of columns in the prediction set do not match the number of columns in the training set!");
				highlightNode(predictionDataLoaderPane.getLabel());
			}

		} else {
			new Alert(this, "Please check the input fields below!");

			for(Node node : errouneousNodeList) {
				highlightNode(node);
			}
			return false;
		}
		return true;
	}

	public List<Validated> getAllTrainingRequiredComponents() {
		List<Validated> validatedComponents = new ArrayList<>();
		validatedComponents.add(trainingDataLoaderPane);
		validatedComponents.add(dataLoaderPane);
		validatedComponents.add(feedForwardPane);
		validatedComponents.add(inputLayerPane);

		for(Validated validated : hiddenLayersPane.getHiddenLayerList()) {
			validatedComponents.add(validated); 
		}
		validatedComponents.add(outputLayerPane);
		validatedComponents.add(backpropagationPane);
		return validatedComponents;
	}

	public void validateAllTrainingRequiredComponents() {
		// Clear previous errors
		errouneousNodeList.clear();

		for(Validated validated : this.getAllTrainingRequiredComponents()) {
			validated.validate();
		}
	}

	public List<Validated> getAllPredictionRequiredComponents() {
		List<Validated> validatedComponents = new ArrayList<>();
		validatedComponents.add(predictionDataLoaderPane);
		return validatedComponents;
	}

	public void validateAllPredictionRequiredComponents() {
		// Clear previous errors
		errouneousNodeList.clear();

		for(Validated validated : this.getAllPredictionRequiredComponents()) {
			validated.validate();
		}
	}

	public void submitErroneousField(Node erroneousNode) {
		errouneousNodeList.add(erroneousNode);
	}

	public List<Node> getErrouneousNodeList() {
		return errouneousNodeList;
	}

	// TODO: Add VBox container to display multiple alerts
	public void dismissAlert() {
		mainScreen.setTop(null);
	}

	public void highlightNode(final Node node) {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				node.setStyle("-fx-effect: dropshadow(three-pass-box, purple, 25, 0, 0, 0);");
			}
		});
	}

	public void resetNode(Node node) {
		node.setStyle("-fx-effect: null;");
	}

	// *** Component Getters / Setters ***

	public Network getNetwork() {
		return network;
	}

	public RootPane getMainScreen() {
		return mainScreen;
	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public ResultsPane getResultsPane() {
		return resultsPane;
	}

	public ChartPane getChartPane() {
		return chartPane;
	}

	public FileChooserPane getTrainingDataLoaderPane() {
		return trainingDataLoaderPane;
	}

	public FileChooserPane getPredictionDataLoaderPane() {
		return predictionDataLoaderPane;
	}

	public DataLoaderPane getDataLoaderPane() {
		return dataLoaderPane;
	}

	public FeedForwardPane getFeedForwardPane() {
		return feedForwardPane;
	}

	public InputLayerPane getInputLayerPane() {
		return inputLayerPane;
	}

	public HiddenLayersPane getHiddenLayersPane() {
		return hiddenLayersPane;
	}

	public OutputLayerPane getOutputLayerPane() {
		return outputLayerPane;
	}

	public BackpropagationPane getBackpropagationPane() {
		return backpropagationPane;
	}

	public Main getMain() {
		return main;
	}

	// Network service

	@Override
	public void registerObserver() {
		this.network.registerObserver(this);
	}

	@Override
	public void unregisterObserver() {
		this.network.unregisterObserver(this);
	}

	@Override
	public void registerStateService(NetworkState state) {		
		controlsPane.registerState(state);
		chartPane.registerState(state);
	}

	@Override
	public void onStateChange(NetworkState state) {
		controlsPane.onStateChange(state);
		chartPane.onStateChange(state);
	}

	@Override
	public void registerIterationService() {
	}

	@Override
	public void registerBatchErrorService() {
	}

	@Override
	public void registerEpochErrorService() {
	} 

	@Override
	public void onIterationChange(int iteration) {
		resultsPane.onIterationChange(iteration);
		chartPane.onIterationChange(iteration);
	}

	@Override
	public void onBatchErrorChange(double error) {
		resultsPane.onEpochErrorChange(error);
		chartPane.onBatchErrorChange(error);
	}

	@Override
	public void onEpochErrorChange(double error) {
		resultsPane.onEpochErrorChange(error);
	}

	@Override
	public void registerValidationProgressService() {
	}

	@Override
	public void registerValidationOutputService() {
		chartPane.registerValidationOutput();
	}

	@Override
	public void registerValidationErrorService() {
	}

	@Override
	public void onValidationProgressChange(double index) {
		resultsPane.onValidationProgressChange(index);
	}

	@Override
	public void onValidationOutputChange(int index, List<Double> validationOutputList) {
		chartPane.onValidationOutputChange(index, validationOutputList);
	}

	@Override
	public void onValidationErrorChange(double error) {
		resultsPane.onValidationErrorChange(error);
	}

	@Override
	public void registerPredictionProgressService() {
	}

	@Override
	public void registerPredictionOutputService() {
	}

	@Override
	public void registerPredictionErrorService() { 
	}

	@Override
	public void onPredictionProgressChange(double predictionProgress) {
		resultsPane.onPredictionProgressChange(predictionProgress);
	}

	@Override
	public void onPredictionOutputChange(int index, List<Double> predictionOutputList) {
		chartPane.onPredictionOutputChange(index, predictionOutputList);
	}
}
