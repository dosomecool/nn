package com.drifft.nn.gui;

import java.io.IOException;

import com.drifft.nn.network.Types.NetworkState;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class ControlsPane extends VBox {

	@FXML
	private VBox rootVBox;

	@FXML
	private Label title;

	@FXML
	private JFXButton initializeButton;

	@FXML
	private JFXButton updateButton;

	@FXML
	private JFXButton resetButton;

	@FXML
	private JFXButton trainButton;

	@FXML
	private JFXButton validateButton;

	@FXML
	private JFXButton predictButton;

	@FXML
	private JFXTextField trainingStatusTextField;

	@FXML
	private JFXTextField validationStatusTextField;

	@FXML
	private JFXTextField predictionStatusTextField;

	private Controller controller;

	private NetworkState networkState;
	private NetworkState trainingState;
	private NetworkState validationState;
	private NetworkState predictionState;

	public ControlsPane(Controller controller) {
		this.controller = controller; 

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Controls.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		trainButton.setDisable(true);
		validateButton.setDisable(true);
		predictButton.setDisable(true);
		updateButton.setDisable(true);
		resetButton.setDisable(true);

		initializeButton.setOnAction( e -> {
			initializeButton.setContentDisplay(ContentDisplay.LEFT);
			initializeButton.setText("INITIALIZE");
			
			if(controller.createNetwork()) {
				// Disable inputs that cannot be updated
				this.controller.getTrainingDataLoaderPane().getSelectButton().setDisable(true);
				this.controller.getTrainingDataLoaderPane().getLabel().setDisable(true);
				this.controller.getInputLayerPane().getNodesTextField().setDisable(true);
				this.controller.getHiddenLayersPane().getAddHiddenLayerButton().setDisable(true);
				this.controller.getHiddenLayersPane().getHiddenLayerList().forEach( hiddenLayer -> {
					hiddenLayer.getNodesTextField().setDisable(true);
					hiddenLayer.getRemoveButton().setDisable(true);
				});
				this.controller.getOutputLayerPane().getNodesTextField().setDisable(true);
			}
		});

		trainButton.setOnAction( e -> {

			if(true) {

				switch(trainingState) {

				case TRAINING_READY:
					controller.getNetwork().train();
					break;
				case TRAINING_STARTED:
					controller.getNetwork().getAtomicStop().compareAndSet(false, true);
					break;
				case TRAINING_SUCCEEDED:
					// user can train again
					controller.getNetwork().train();
					break;
				case TRAINING_STOPPED:
					// user can train again 
					controller.getNetwork().train();
					break;
				case TRAINING_FAILED:
					new Alert(controller, "The training process failed. Please update the model and try again.", true);
					break;
				default:
					break;
				}
			} 			
		});

		validateButton.setOnAction( e -> {

			if(networkState == NetworkState.NETWORK_INITIALIZED) {	
				controller.getChartPane().clearChart("validate"); 
				controller.getNetwork().validate();
			}
		});

		predictButton.setOnAction( e -> {

			predictButton.setContentDisplay(ContentDisplay.LEFT);
			predictButton.setText("PREDICT");

			if(controller.createPredictionData()) {
				controller.getChartPane().clearChart("predict"); 
				controller.getNetwork().predict();
			}
		});

		updateButton.setOnAction( e -> {

			if(!controller.getNetwork().isStopped()) {
				controller.getNetwork().getAtomicStop().compareAndSet(false, true);
			}
			this.controller.updateNetwork();
		});

		resetButton.setOnAction( e -> {

			if(!controller.getNetwork().isStopped()) {
				controller.getNetwork().getAtomicStop().compareAndSet(false, true);
			}
			this.controller.getMain().reset();
		});
	}

	public void setTitle(String title) {
		this.title.setText(title);
	}

	public void registerState(NetworkState state) {		

		if(state == null) {
			networkState = NetworkState.NETWORK_FAILED;
			return;
		}

		if((networkState = state) == NetworkState.NETWORK_INITIALIZED) {
			initializeButton.setContentDisplay(ContentDisplay.TEXT_ONLY);
			initializeButton.setText("INITIALIZE");
			initializeButton.setDisable(true);

			trainButton.setDisable(false);
			validateButton.setDisable(false);
			predictButton.setDisable(false);
			updateButton.setDisable(false);
			resetButton.setDisable(false);

			trainingState = NetworkState.TRAINING_READY;
		}
	}

	public void onStateChange(NetworkState state) {

		if(state == null) {
			networkState = NetworkState.NETWORK_FAILED;
			return;
		}

		switch(state) {

		// training 
		case TRAINING_STARTED: 
			trainingState = state;
			updateButtonLabel(trainButton, "STOP");
			trainingStatusTextField.setText(trainingState.toString().replace("_", " "));
			break;
		case TRAINING_SUCCEEDED:
			trainingState = state;
			updateButtonLabel(trainButton, "TRAIN AGAIN");
			trainingStatusTextField.setText(trainingState.toString().replace("_", " "));
			break;
		case TRAINING_STOPPED:
			trainingState = state;
			updateButtonLabel(trainButton, "RESUME");
			trainingStatusTextField.setText(trainingState.toString().replace("_", " "));
			break;
		case TRAINING_FAILED:
			trainingState = state;
			updateButtonLabel(trainButton, "FAILED");
			trainingStatusTextField.setText(trainingState.toString().replace("_", " "));

			trainButton.setDisable(true);
			validateButton.setDisable(true);
			predictButton.setDisable(true);
			break;

			// validation	
		case VALIDATION_STARTED:
			validationState = state;
			validationStatusTextField.setText(validationState.toString().replace("_", " "));
			break;
		case VALIDATION_SUCCEEDED:
			validationState = state;
			updateButtonLabel(validateButton, "VALIDATE AGAIN");
			validationStatusTextField.setText(validationState.toString().replace("_", " "));
			break;
		case VALIDATION_FAILED:
			validationState = state;
			validationStatusTextField.setText(validationState.toString().replace("_", " "));
			validateButton.setDisable(true);
			break;

			// prediction
		case PREDICTION_STARTED:
			predictionState = state;
			predictionStatusTextField.setText(predictionState.toString().replace("_", " "));

			Platform.runLater( new Runnable() {

				@Override
				public void run() {
					predictButton.setContentDisplay(ContentDisplay.TEXT_ONLY);
				}
			});
			break;
		case PREDICTION_SUCCEEDED:
			predictionState = state;
			updateButtonLabel(predictButton, "PREDICT AGAIN");
			predictionStatusTextField.setText(predictionState.toString().replace("_", " "));
			break;
		case PREDICTION_FAILED:
			predictionState = state;
			predictionStatusTextField.setText(predictionState.toString().replace("_", " "));
			predictButton.setDisable(true);
			break;	

		case NETWORK_FAILED:	

			trainButton.setDisable(true);
			validateButton.setDisable(true);
			predictButton.setDisable(true);

			break;
		case NETWORK_FINISHED:
			// clean up
			break;
		default:
			break;
		}
	}

	private void updateButtonLabel(final JFXButton button, final String text) {

		Platform.runLater( new Runnable() {

			@Override
			public void run() {
				button.setText(text);
			}
		});
	}
}
