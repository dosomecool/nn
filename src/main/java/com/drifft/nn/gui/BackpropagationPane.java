package com.drifft.nn.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.nn.network.NetworkInput.Parameter;
import com.drifft.nn.network.Types.OptimizationType;
import com.drifft.nn.network.Types.ParameterType;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.DoubleValidator;
import com.jfoenix.validation.IntegerValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class BackpropagationPane extends VBox implements Validated {

	@FXML
	private Label label;

	@FXML
	private JFXTextField batchTextField;

	@FXML
	private JFXTextField epochsTextField;

	@FXML
	private JFXTextField threadsTextField;

	@FXML
	private JFXComboBox<OptimizationType> optimizerDropDown;

	@FXML
	private JFXTextField paramOneTextField;

	@FXML
	private JFXTextField paramTwoTextField;

	@FXML
	private VBox parametersContainer;

	private List<JFXTextField> parameterTextFieldList = new ArrayList<>();
	private List<Parameter> parameters = new ArrayList<>();

	private Integer batchTextFieldValue;
	private Integer epochsTextFieldValue;
	private Integer threadsTextFieldValue;
	private OptimizationType optimizerDropDownValue;
	private Double paramOneTextFieldValue;
	private Double paramTwoTextFieldValue;

	private Controller controller;

	public BackpropagationPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Backpropagation.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		parameterTextFieldList.add(paramOneTextField);
		parameterTextFieldList.add(paramTwoTextField);

		// Optimizer
		for (OptimizationType type : OptimizationType.values()) {
			optimizerDropDown.getItems().add(type);
		}

		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("INPUT IS REQUIRED");

		IntegerValidator integerValidator = new IntegerValidator();
		integerValidator.setMessage("INPUT IS NOT AN INTEGER");

		DoubleValidator doubleValidator = new DoubleValidator();
		doubleValidator.setMessage("INPUT IS NOT NUMERIC");

		// Batch
		batchTextField.getValidators().addAll(requiredvalidator, integerValidator);
		batchTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if (!newValue) {
				batchTextField.validate();
			}
		});

		batchTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				batchTextField.setStyle("-fx-effect: null;");

				try {
					batchTextFieldValue = Integer.parseInt(newValue);
				} catch (NumberFormatException ex) {
					// No action necessary
				}
			}
		});

		// Epochs
		epochsTextField.getValidators().addAll(requiredvalidator, integerValidator);
		epochsTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if (!newValue) {
				epochsTextField.validate();
			}
		});

		epochsTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				epochsTextField.setStyle("-fx-effect: null;");

				try {
					epochsTextFieldValue = Integer.parseInt(newValue);
				} catch (NumberFormatException ex) {
					// No action necessary
				}
			}
		});

		// Threads
		threadsTextField.getValidators().addAll(requiredvalidator, integerValidator);
		threadsTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if (!newValue) {
				threadsTextField.validate();
			}
		});

		threadsTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				threadsTextField.setStyle("-fx-effect: null;");

				try {
					threadsTextFieldValue = Integer.parseInt(newValue);
				} catch (NumberFormatException ex) {
					// No action necessary
				}
			}
		});

		// Optimizer
		optimizerDropDown.getValidators().addAll(requiredvalidator);
		optimizerDropDown.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if (!newValue) {
				optimizerDropDown.validate();
			}
		});

		optimizerDropDown.valueProperty().addListener((observable, oldValue, newValue) -> {
			optimizerDropDown.setStyle("-fx-effect: null;");

			if (newValue != null) {
				optimizerDropDownValue = newValue;
				parameters.clear();
				addParameters(optimizerDropDownValue);
			}
		});

		// set listeners here and on value change create new parameters
		paramOneTextField.getValidators().addAll(requiredvalidator, doubleValidator);
		paramOneTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if(!newValue) {
				paramOneTextField.validate();
			} 
		});

		paramOneTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			paramOneTextField.setStyle("-fx-effect: null;");

			try {
				ParameterType parameterType = ParameterType.valueOf(paramOneTextField.getId());
				paramOneTextFieldValue = Double.parseDouble(newValue);
				parameters.add(new Parameter(parameterType, paramOneTextFieldValue));
			} catch(NumberFormatException ex) {
				// No action necessary
			}
		});	

		paramTwoTextField.getValidators().addAll(requiredvalidator, doubleValidator);
		paramTwoTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if(!newValue) {
				paramTwoTextField.validate();
			} 
		});

		paramTwoTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			paramTwoTextField.setStyle("-fx-effect: null;");

			try {
				ParameterType parameterType = ParameterType.valueOf(paramTwoTextField.getId());
				paramTwoTextFieldValue = Double.parseDouble(newValue);
				parameters.add(new Parameter(parameterType, paramTwoTextFieldValue));
			} catch(NumberFormatException ex) {
				// No action necessary
			}
		});	
	}

	private void addParameters(OptimizationType optimizationType) {

		parameterTextFieldList.forEach( textField -> {
			textField.setText("");
			textField.setPromptText("");
			textField.setDisable(true);
			paramOneTextFieldValue = null;
			paramTwoTextFieldValue = null;
		});

		for(int i=0; i<optimizationType.getParameterTypes().size(); i++) {
			String parameterName = optimizationType.getParameterTypes().get(i).toString().replace("_", " ");
			parameterTextFieldList.get(i).setPromptText(parameterName);
			parameterTextFieldList.get(i).setDisable(false);
			parameterTextFieldList.get(i).setId(optimizationType.getParameterTypes().get(i).toString());
		}
	}

	public void setTitle(String title) {
		this.label.setText(title);
	}

	public void validate() {

		for (ValidatorBase validator : batchTextField.getValidators()) {

			if (validator.hasErrorsProperty().getValue() || batchTextFieldValue == null) {
				controller.submitErroneousField(batchTextField);
			}
		}

		for (ValidatorBase validator : epochsTextField.getValidators()) {

			if (validator.hasErrorsProperty().getValue() || epochsTextFieldValue == null) {
				controller.submitErroneousField(epochsTextField);
			}
		}

		for (ValidatorBase validator : threadsTextField.getValidators()) {

			if (validator.hasErrorsProperty().getValue() || threadsTextFieldValue == null) {
				controller.submitErroneousField(threadsTextField);
			}
		}

		for (ValidatorBase validator : optimizerDropDown.getValidators()) {

			if (validator.hasErrorsProperty().getValue() || optimizerDropDownValue == null) {
				controller.submitErroneousField(optimizerDropDown);
			}
		}

		if(!paramOneTextField.isDisabled()) {

			for (ValidatorBase validator : paramOneTextField.getValidators()) {

				if (validator.hasErrorsProperty().getValue() || paramOneTextFieldValue == null) {
					controller.submitErroneousField(paramOneTextField);
				}
			}
		}

		if(!paramTwoTextField.isDisabled()) {

			for (ValidatorBase validator : paramTwoTextField.getValidators()) {	

				if (validator.hasErrorsProperty().getValue() || paramTwoTextFieldValue == null) {
					controller.submitErroneousField(paramTwoTextField);
				}
			}
		}
	}

	// *** Getter / Setters ***

	public JFXTextField getBatchTextField() {
		return batchTextField;
	}

	public JFXTextField getEpochsTextField() {
		return epochsTextField;
	}

	public JFXTextField getThreadsTextField() {
		return threadsTextField;
	}

	public JFXComboBox<OptimizationType> getOptimizerDropDown() {
		return optimizerDropDown;
	}

	public int getBatchTextFieldValue() {
		return batchTextFieldValue;
	}

	public int getEpochsTextFieldValue() {
		return epochsTextFieldValue;
	}

	public int getThreadsTextFieldValue() {
		return threadsTextFieldValue;
	}

	public OptimizationType getOptimizerDropDownValue() {
		return optimizerDropDownValue;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}	
}
