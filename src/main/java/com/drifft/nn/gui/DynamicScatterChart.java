package com.drifft.nn.gui;

import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

// https://stackoverflow.com/questions/44536365/how-can-i-display-value-while-hovering-on-the-scatter-chart-points-in-javafx

public class DynamicScatterChart {

	// Size
	private final static double WIDTH = 480;
	private final static double HEIGHT = 320;
	private final static int MAX_POINTS = 2500;
	private final static int XAXIS_TICK_UNITS = 250;
	
	private final static int MAX_SERIES_INDEX = 2;

	@SuppressWarnings("unused")
	private String xAxisLabel;
	@SuppressWarnings("unused")
	private String yAxisLabel;

	// Defining the axes
	final NumberAxis xAxis = new NumberAxis();
	final NumberAxis yAxis = new NumberAxis();

	private IntegerProperty totalPointsProperty = new SimpleIntegerProperty(MAX_POINTS);
	private IntegerProperty xAxisTickUnitsProperty = new SimpleIntegerProperty(XAXIS_TICK_UNITS);

	// scatter chart	
	private ObservableList<XYChart.Series<Double, Double>> scatterChartData = FXCollections.observableArrayList();
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private final ScatterChart scatterChart = new ScatterChart(xAxis, yAxis, scatterChartData);

	// Chart name
	@SuppressWarnings("unused")
	private String name; 
	
	public DynamicScatterChart(String name, String xAxisLabel, String yAxisLabel) {
		this.name = name;
		this.xAxisLabel = xAxisLabel;
		this.yAxisLabel = yAxisLabel;

		// x-axis
		xAxis.setLabel(xAxisLabel);
		xAxis.setMinorTickVisible(false);
		xAxis.setAutoRanging(true);
		xAxis.setLowerBound(0);
//		xAxis.setUpperBound(this.totalPointsProperty.get());
		xAxis.setTickUnit(this.xAxisTickUnitsProperty.get());

		// y-axis
		yAxis.setLabel(yAxisLabel);		
		yAxis.setMinorTickVisible(false);
		yAxis.setAutoRanging(true);

		// Set chart properties		
		scatterChart.setAnimated(false);
		scatterChart.setLegendVisible(false);

		scatterChart.setMinSize(WIDTH, HEIGHT);
		scatterChart.setMaxSize(WIDTH, HEIGHT);
		
		scatterChart.setCache(true);
		
		for(int i=0; i<MAX_SERIES_INDEX; i++) {
			addSeries(name + "-" + i);
		}
	}

	// Read more
	// https://docs.oracle.com/javafx/2/charts/scatter-chart.htm

	private void addSeries(final String name) {
		// Chart data that goes into one series
		ObservableList<XYChart.Data<Double, Double>> observableDataList = FXCollections.observableArrayList();
		// Chart series
		Series<Double, Double> series = new ScatterChart.Series<Double, Double>(name, observableDataList);
		scatterChartData.add(series);	
	}

	public void updateChart(int series, double x, List<Double> ys) {
				
		if(series > (MAX_SERIES_INDEX-1)) {
			series = series % MAX_SERIES_INDEX;
		}
		
        for(int i=0; i<ys.size(); i++) {
        	double y = ys.get(i);
			scatterChartData.get(series).getData().add(new XYChart.Data<Double, Double>(x, y));
		}
	}
	
	public void clearData(int series) {
		scatterChartData.get(series).getData().clear();
	}

	// *** Getters / Setters ***

	public IntegerProperty getTotalPointsProperty() {
		return totalPointsProperty;
	}

	public IntegerProperty getxAxisTickUnitsProperty() {
		return xAxisTickUnitsProperty;
	}

	@SuppressWarnings("rawtypes")
	public ScatterChart getChart() {
		return scatterChart;
	}
}
