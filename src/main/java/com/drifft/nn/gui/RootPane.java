package com.drifft.nn.gui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class RootPane extends BorderPane {

	//	Image roses = new Image(getClass().getResourceAsStream("roses.jpg"));

    @FXML
    private BorderPane root;

    @FXML
    private VBox centerVBox;

    @FXML
    private AnchorPane leftAnchorPane;

    @FXML
    private VBox leftVBox;

    @FXML
    private AnchorPane rightAnchorPane;

    @FXML
    private VBox rightVBox;

	@SuppressWarnings("unused")
	private Controller controller; 

	public RootPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Root.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		root.getStylesheets().addAll(getClass().getResource("DrifftStylesheet.css").toExternalForm());
	}

	@FXML
	public void initialize() {
	}

	public BorderPane getRoot() {
		return root;
	}

	public VBox getLeftVBox() {
		return leftVBox;
	}

	public VBox getRightVBox() {
		return rightVBox;
	}

	public VBox getCenterVBox() {
		return centerVBox;
	}	
}
