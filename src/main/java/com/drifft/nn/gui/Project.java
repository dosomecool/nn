package com.drifft.nn.gui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.nn.gui.DataLoaderPane.ShuffleType;
import com.drifft.nn.network.Types.ActivationType;
import com.drifft.nn.network.Types.CostFunctionType;
import com.drifft.nn.network.Types.NormalizationType;
import com.drifft.nn.network.Types.OptimizationType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Project {

	// File chooser
	private File trainingDataFile; 
	
	// Data loader
	private Double trainingPercent; 
	private Long dataLoaderSeed; 
	private ShuffleType shuffleType;
	
	// Feedforward
	private Double variance;
	private Long feedForwardSeed;
	private CostFunctionType costFunctionType;
	
	// Input layer
	private Integer inputNodes;
	private NormalizationType normalizationType; 
	
	// Hidden layers
	private List<HiddenLayerValues> hiddenLayers = new ArrayList<>();
	
	// Output layer
	private Integer outputNodes;
	private ActivationType outputActivationFunction; 

	// Backpropagation
	private Integer batch;
	private Integer epochs;
	private Integer threads;
	private OptimizationType optimizationType;
	
	public Project() {
	}
	
	public Project(File trainingDataFile, 
			Double trainingPercent, 
			Long dataLoaderSeed, 
			ShuffleType shuffleType,
			Double variance,
			Long feedForwardSeed,
			CostFunctionType costFunctionType,
			Integer inputNodes,
			NormalizationType normalizationType,
			List<HiddenLayerValues> hiddenLayers,
			Integer outputNodes,
			ActivationType outputActivationFunction, 
			Integer batch,
			Integer epochs,
			Integer threads,
			OptimizationType optimizationType) {
		this.trainingDataFile = trainingDataFile; 
		this.trainingPercent = trainingPercent; 
		this.dataLoaderSeed = dataLoaderSeed; 
		this.shuffleType = shuffleType;
		this.variance = variance;
		this.feedForwardSeed = feedForwardSeed;
		this.costFunctionType = costFunctionType;
		this.inputNodes = inputNodes;
		this.normalizationType = normalizationType; 
		this.hiddenLayers = hiddenLayers;
		this.outputNodes = outputNodes;
		this.outputActivationFunction = outputActivationFunction; 
		this.batch = batch;
		this.epochs = epochs;
		this.threads = threads;
		this.optimizationType = optimizationType;
	}
		
	public File getTrainingDataFile() {
		return trainingDataFile;
	}

	public void setTrainingDataFile(File trainingDataFile) {
		this.trainingDataFile = trainingDataFile;
	}

	public Double getTrainingPercent() {
		return trainingPercent;
	}

	public void setTrainingPercent(Double trainingPercent) {
		this.trainingPercent = trainingPercent;
	}

	public Long getDataLoaderSeed() {
		return dataLoaderSeed;
	}

	public void setDataLoaderSeed(Long dataLoaderSeed) {
		this.dataLoaderSeed = dataLoaderSeed;
	}

	public ShuffleType getShuffleType() {
		return shuffleType;
	}

	public void setShuffleType(ShuffleType shuffleType) {
		this.shuffleType = shuffleType;
	}

	public Double getVariance() {
		return variance;
	}

	public void setVariance(Double variance) {
		this.variance = variance;
	}

	public Long getfeedForwardSeed() {
		return feedForwardSeed;
	}

	public void setfeedForwardSeed(Long feedForwardSeed) {
		this.feedForwardSeed = feedForwardSeed;
	}

	public CostFunctionType getCostFunctionType() {
		return costFunctionType;
	}

	public void setCostFunctionType(CostFunctionType costFunctionType) {
		this.costFunctionType = costFunctionType;
	}

	public Integer getInputNodes() {
		return inputNodes;
	}

	public void setInputNodes(Integer inputNodes) {
		this.inputNodes = inputNodes;
	}

	public NormalizationType getNormalizationType() {
		return normalizationType;
	}

	public void setNormalizationType(NormalizationType normalizationType) {
		this.normalizationType = normalizationType;
	}

	public List<HiddenLayerValues> getHiddenLayers() {
		return hiddenLayers;
	}

	public void setHiddenLayers(List<HiddenLayerValues> hiddenLayers) {
		this.hiddenLayers = hiddenLayers;
	}

	public Integer getOutputNodes() {
		return outputNodes;
	}

	public void setOutputNodes(Integer outputNodes) {
		this.outputNodes = outputNodes;
	}

	public ActivationType getOutputActivationFunction() {
		return outputActivationFunction;
	}

	public void setOutputActivationFunction(ActivationType outputActivationFunction) {
		this.outputActivationFunction = outputActivationFunction;
	}

	public Integer getBatch() {
		return batch;
	}

	public void setBatch(Integer batch) {
		this.batch = batch;
	}

	public Integer getEpochs() {
		return epochs;
	}

	public void setEpochs(Integer epochs) {
		this.epochs = epochs;
	}

	public Integer getThreads() {
		return threads;
	}

	public void setThreads(Integer threads) {
		this.threads = threads;
	}
	public OptimizationType getOptimizationType() {
		return optimizationType;
	}

	public void setOptimizationType(OptimizationType optimizationType) {
		this.optimizationType = optimizationType;
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("trainingDataFile: ").append(trainingDataFile);
		sb.append("\ntrainingPercent: ").append(trainingPercent);
		sb.append("\ndataLoaderSeed: ").append(dataLoaderSeed);
		sb.append("\nshuffleType: ").append(shuffleType);
		sb.append("\nvariance: ").append(variance);
		sb.append("\nfeedForwardSeed: ").append(feedForwardSeed);
		sb.append("\ncostFunctionType: ").append(costFunctionType);
		sb.append("\ninputNodes: ").append(inputNodes);
		sb.append("\nnormalizationType: ").append(normalizationType);
		sb.append("\nhiddenLayers: ").append(hiddenLayers);
		sb.append("\noutputNodes: ").append(outputNodes);
		sb.append("\noutputActivationFunction: ").append(outputActivationFunction);
		sb.append("\nbatch: ").append(batch);
		sb.append("\nepochs: ").append(epochs);
		sb.append("\nthreads: ").append(threads);
		sb.append("\noptimizationType: ").append(optimizationType);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}

	public static class HiddenLayerValues {
		
		private Integer index;
		private Integer hiddenNodes; 
		private ActivationType hiddenLayerActivationFunction;
		
		public HiddenLayerValues() {
		}
		
		public HiddenLayerValues(Integer index, Integer hiddenNodes, ActivationType hiddenLayerActivationFunction) {
			this.index = index;
			this.hiddenNodes = hiddenNodes;
			this.hiddenLayerActivationFunction = hiddenLayerActivationFunction;
		}
		
		public Integer getIndex() {
			return index;
		}

		public void setIndex(Integer index) {
			this.index = index;
		}

		public Integer getHiddenNodes() {
			return hiddenNodes;
		}

		public void setHiddenNodes(Integer hiddenNodes) {
			this.hiddenNodes = hiddenNodes;
		}

		public ActivationType getHiddenLayerActivationFunction() {
			return hiddenLayerActivationFunction;
		}

		public void setHiddenLayerActivationFunction(ActivationType hiddenLayerActivationFunction) {
			this.hiddenLayerActivationFunction = hiddenLayerActivationFunction;
		}
		
		@Override 
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("hiddenNodes: ").append(hiddenNodes);
			sb.append("\nhiddenLayerActivationFunction: ").append(hiddenLayerActivationFunction);

			String jsonStr = null; 
			ObjectMapper mapper = new ObjectMapper();
			
	        try {
	            jsonStr = mapper.writeValueAsString(this);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
			return jsonStr;
		}
	}
}
