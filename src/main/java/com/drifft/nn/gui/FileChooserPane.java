package com.drifft.nn.gui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class FileChooserPane extends VBox implements Validated {

    @FXML
    private VBox rootVBox;

    @FXML
    private Label title;

    @FXML
    private JFXButton selectButton;

    @FXML
    private JFXTextField label;

    @FXML
    private JFXButton openButton;
    
	private File file;  
	private static String directory = "";
	private static String filename = "";

	private final FileChooser fileChooser = new FileChooser();
	private static Desktop desktop = Desktop.getDesktop();

	private Controller controller; 
	
	private int rows; 
	
	public FileChooserPane(Controller controller) {
		this.controller = controller;
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("FileChooser.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void initialize() {
		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("DATA FILE IS REQUIRED");
		
		label.getValidators().addAll(requiredvalidator);
		selectButton.focusedProperty().addListener((observable, oldValue, newValue) -> {

			if(!newValue) {
				label.validate();
			} 
		});
		
		selectButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(final ActionEvent e) {

				configureFileChooser("SELECT FILE", fileChooser);
				Stage stage = (Stage) selectButton.getScene().getWindow();
				file = fileChooser.showOpenDialog(stage);

				if (file != null) {
					setFilename(file);
					label.appendText(filename.toUpperCase());
				}
			}
		});

		openButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(final ActionEvent e) {

				if (file != null) {
					openFile(file);
				}
			}
		});
		
		label.textProperty().addListener( e -> {
			label.setStyle("-fx-effect: null;");
		});
	}
	
	private static void setFilename(File file) {
		Path path = Paths.get(file.toString());
		directory = path.getParent().toString();
		filename = path.getFileName().toString();
	}

	private static void configureFileChooser(String title, final FileChooser fileChooser) {
		fileChooser.setTitle(title);
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home"))); 
		if(!directory.equals("")) {
			fileChooser.setInitialDirectory(new File(directory)); 
		}        
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
	}

	private static void openFile(File file) {
		try {
			desktop.open(file);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void validate() {

		for(ValidatorBase validator: label.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || file == null) {
				controller.submitErroneousField(label);
			}
		}
	}
	
	public void setTitle(String title) {
		this.title.setText(title);
	}
	
	public String getPath() {
		return file.toString();
	}
	
	public void setFile(File file) {
		this.file = file;
		if (file != null) {
			setFilename(file);
			label.appendText(filename.toUpperCase());
		}
	}
	
	// *** Getters / Setters ***
	
	public File getFile() {
		return file;
	}
	
	public static String getFilename() {
		return filename;
	}
	
	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public JFXTextField getLabel() {
		return label;
	}
	
	public JFXButton getSelectButton() {
		return selectButton;
	}
	public VBox getContainer() {
		return rootVBox;
	}
}
