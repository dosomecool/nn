package com.drifft.nn.gui;

import java.io.IOException;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.DoubleValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import com.jfoenix.validation.base.ValidatorBase;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class DataLoaderPane extends VBox implements Validated {

	@FXML
	private Label label;

	@FXML
	private JFXTextField trainingPercentTextField;

	@FXML
	private JFXTextField seedTextField;

	@FXML
	private JFXComboBox<ShuffleType> shuffleDropDown;

	private Controller controller;

	// Values
	private Double trainingPercentTextFieldValue; 
	private Long seedTextFieldValue;
	private Boolean shuffleDropDownValue; 
	
	public DataLoaderPane(Controller controller) {
		this.controller = controller;

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("DataLoader.fxml"));
			loader.setController(this);
			loader.setRoot(this);
			loader.load();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() {
		// Shuffle
		for (ShuffleType type : ShuffleType.values()) {
			shuffleDropDown.getItems().add(type);
		}
		
		RequiredFieldValidator requiredvalidator = new RequiredFieldValidator();
		requiredvalidator.setMessage("INPUT IS REQUIRED");
		DoubleValidator doubleValidator = new DoubleValidator();
		doubleValidator.setMessage("INPUT IS NOT NUMERIC");
		RangeValidator rangeValidator = new RangeValidator(0, 1);
		
		trainingPercentTextField.getValidators().addAll(requiredvalidator, doubleValidator, rangeValidator);
		trainingPercentTextField.focusedProperty().addListener((observable, oldValue, newValue)->{
			
		    if(!newValue) {
		    	trainingPercentTextField.validate();
		    } 
		});
		
		trainingPercentTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			trainingPercentTextField.setStyle("-fx-effect: null;");
			
			try {
				trainingPercentTextFieldValue = Double.parseDouble(newValue);
			} catch(NumberFormatException ex) {
				// No action necessary
			}
		});	
		
		seedTextField.getValidators().addAll(requiredvalidator, doubleValidator);
		seedTextField.focusedProperty().addListener((observable, oldValue, newValue)->{
			
		    if(!newValue) {
		    	seedTextField.validate();
		    } 
		});
		
		seedTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			seedTextField.setStyle("-fx-effect: null;");
			
			try {
				seedTextFieldValue = Long.parseLong(newValue);
			} catch(NumberFormatException ex) {
				// No action necessary
			}
		});
		
		shuffleDropDown.getValidators().add(requiredvalidator);
		shuffleDropDown.focusedProperty().addListener((observable, oldValue, newValue)->{
			
		    if(!newValue) {
		    	shuffleDropDown.validate();
		    } 
		});
		
		shuffleDropDown.valueProperty().addListener((observable, oldValue, newValue) -> {
			shuffleDropDown.setStyle("-fx-effect: null;");
			
			if(newValue == ShuffleType.TRUE) {
				shuffleDropDownValue = true;
			} else if (newValue == ShuffleType.FALSE){
				shuffleDropDownValue = false;
			}
		});
	}
	
	public boolean isShuffled() {
		return shuffleDropDownValue;
	}

	public void setTitle(String title) {
		this.label.setText(title);
	}

	@Override
	public void validate() {

		for(ValidatorBase validator: trainingPercentTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || trainingPercentTextFieldValue == null) {
				controller.submitErroneousField(trainingPercentTextField);
			}
		}
		
		for(ValidatorBase validator: seedTextField.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || seedTextFieldValue == null) {
				controller.submitErroneousField(seedTextField);
			}
		}
		
		for(ValidatorBase validator: shuffleDropDown.getValidators()) {
			
			if(validator.hasErrorsProperty().getValue() || shuffleDropDownValue == null) {
				controller.submitErroneousField(shuffleDropDown);
			} 
		}
	}
	
	public enum ShuffleType {
		TRUE, FALSE
	}
	
	// *** Getters / Setters *** 
	
	public JFXTextField getTrainingPercentTextField() {
		return trainingPercentTextField;
	}
	
	public double getTrainingPercentTextFieldValue() {
		return trainingPercentTextFieldValue;
	}
	
	public JFXTextField getSeedTextField() {
		return seedTextField;
	}
	
	public long getSeedTextFieldValue() {
		return seedTextFieldValue;
	}

	public JFXComboBox<ShuffleType> getShuffleDropDown() {
		return shuffleDropDown;
	}

	public Boolean getShuffleDropDownValue() {
		return shuffleDropDownValue;
	}
}
