package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class Gaussian implements ActivationFunction {

	public Gaussian() {
	}
	
	public double getFunctionValue(double x) {
		return Math.exp(-1.0 * Math.pow(x, 2));
	}

	public double getFunctionDerivativeValue(double x) {
		return -2.0 * x * getFunctionValue(x);
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Gaussian activation function does not support this method!");
		}
		return 1d;
	}

	public ActivationType getActivationType() {
		return ActivationType.GAUSSIAN;
	}
}
