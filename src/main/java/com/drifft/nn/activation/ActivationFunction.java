package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public interface ActivationFunction {

	public double getFunctionValue(double x);
	public double getFunctionDerivativeValue(double x);
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex);
	public ActivationType getActivationType();
}
