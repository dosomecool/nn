package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class Linear implements ActivationFunction {

	public double getFunctionValue(double x) {
		return x;
	}

	public double getFunctionDerivativeValue(double x) {
		return 1.0;
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Linear activation function does not support this method!");
		}
		return 1d;
	}

	public ActivationType getActivationType() {
		return ActivationType.LINEAR;
	}
}
