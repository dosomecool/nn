package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class Elu implements ActivationFunction {

	// https://github.com/aleju/papers/blob/master/neural-nets/ELUs.md
	
	double alpha = 0.5;
	
	public Elu() {
	}
	
	public double getFunctionValue(double x) {
		if( x > 0 ) return x;
		return alpha * ( Math.exp(x) - 1 );
	}

	public double getFunctionDerivativeValue(double x) {
		if( x > 0 ) return 1.0;
		return getFunctionValue(x) + alpha;
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Elu activation function does not support this method!");
		}
		return 1d;
	}

	public ActivationType getActivationType() {
		return ActivationType.ELU;
	}
}
