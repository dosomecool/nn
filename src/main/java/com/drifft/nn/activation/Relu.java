package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class Relu implements ActivationFunction {

	public Relu() {
	}
	
	public double getFunctionValue(double x) {
		return Math.log( 1.0 + Math.exp(x) ); // RELU approximation
	}

	public double getFunctionDerivativeValue(double x) {
		return ( 1 / (1 + Math.exp(-x)) );
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Relu activation function does not support this method!");
		}
		return 1d;
	}
	
	public ActivationType getActivationType() {
		return ActivationType.RELU;
	}
}
