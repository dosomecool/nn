package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class LeakyRelu implements ActivationFunction{

	double alpha = 0.33;
	
	public LeakyRelu() {
	}
	
	public double getFunctionValue(double x) {
		if( x > 0 ) return x;
		return alpha * x;
	}

	public double getFunctionDerivativeValue(double x) {	
		if( x > 0 ) return 1.0;
		return alpha;
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Leaky Relu activation function does not support this method!");
		}
		return 1d;
	}

	public ActivationType getActivationType() {
		return ActivationType.LEAKY_RELU;
	}
}
