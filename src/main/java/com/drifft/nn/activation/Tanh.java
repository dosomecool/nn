package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class Tanh implements ActivationFunction {

	// http://mochajl.readthedocs.io/en/latest/user-guide/neuron.html
	
	public Tanh() {
	}
	
	public double getFunctionValue(double x) {
		return ( 2 / (1 + Math.exp(-2 * x)) ) - 1;
	}

	public double getFunctionDerivativeValue(double x) {
		return ( 1 - Math.pow(getFunctionValue(x), 2) );	// (4 * Math.exp(2*x)) / Math.pow((Math.exp(2*x) + 1), 2)
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Tanh activation function does not support this method!");
		}
		return 1d;
	}

	public ActivationType getActivationType() {
		return ActivationType.TANH;
	}
}
