package com.drifft.nn.activation;

import com.drifft.nn.network.Layer;
import com.drifft.nn.network.Types.ActivationType;
import com.drifft.nn.network.Types.NodeType;

public class ActivationFunctionFactory {

    private Layer layer;
    
    public ActivationFunctionFactory() {
    }
    
    public ActivationFunctionFactory(Layer layer) {
        this.layer = layer;
    }
	
	public ActivationFunction get(ActivationType activation){

		if(activation == null){
			return null;
		}	
		if(activation == ActivationType.ELU){
			return new Elu();
			
		} else if(activation == ActivationType.LEAKY_RELU){
			return new LeakyRelu();
			
		} else if(activation == ActivationType.RELU){
			return new Relu();
			
		} else if(activation == ActivationType.TANH){
			return new Tanh();
			
		} else if(activation == ActivationType.SIGMOID){
			return new Sigmoid();

		} else if (activation == ActivationType.LINEAR) {
			return new Linear();
			
		} else if (activation == ActivationType.GAUSSIAN) {			
			return new Gaussian();
		
		} else if (activation == ActivationType.SOFTMAX) {
		
			if (layer == null || layer.getNodeType().equals(NodeType.HIDDEN)) {
				throw new IllegalStateException("Softmax activation can be used for output layer only!");
			}
			this.layer.setConnected(true);
			return new Softmax(this.layer);
		}
		return null;
	}
}
