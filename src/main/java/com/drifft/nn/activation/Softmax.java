package com.drifft.nn.activation;

import com.drifft.nn.network.Layer;
import com.drifft.nn.network.Node;
import com.drifft.nn.network.Types.ActivationType;

// Source: https://deepnotes.io/softmax-crossentropy

public class Softmax implements ActivationFunction {

    private Layer layer;

    public Softmax(Layer layer) {
        this.layer = layer;
    }

	@Override
	public double getFunctionValue(double x) {
        double sum = 0;
        double max = 0;
        
        for (Node node : layer.getNodeList()) {
            if(node.getValue() > max) {
            	max = node.getValue(); 
            }
        }
        
        for (Node node : layer.getNodeList()) {
            sum += Math.exp(node.getValue() - max);
        }
        return Math.exp(x - max) / sum;
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double x) {
		
		if (true) {
			throw new IllegalStateException("The Softmax activation function does not support this method!");
		}
		return 1d;
	}

	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if(isSameIndex) {
			return getFunctionValue(xi) * (1d - getFunctionValue(xj));
			
		} else {
			return -1d * getFunctionValue(xi) * getFunctionValue(xj);
		}
	}

	@Override
	public ActivationType getActivationType() {
		return ActivationType.SOFTMAX;
	}
}
