package com.drifft.nn.activation;

import com.drifft.nn.network.Types.ActivationType;

public class Sigmoid implements ActivationFunction{

	public Sigmoid() {
	}
	
	public double getFunctionValue(double x) {
		return ( 1 / ( 1 + Math.exp(-x)) );
	}
	
	public double getFunctionDerivativeValue(double x) {
		return ( getFunctionValue(x) * ( 1.0 - getFunctionValue(x)) );
	}
	
	@SuppressWarnings("unused")
	@Override
	public double getFunctionDerivativeValue(double xi, double xj, boolean isSameIndex) {
		
		if (true) {
			throw new IllegalStateException("The Sigmoid activation function does not support this method!");
		}
		return 1d;
	}

	public ActivationType getActivationType() {
		return ActivationType.SIGMOID;
	}
}
