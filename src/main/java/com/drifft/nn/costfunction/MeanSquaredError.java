package com.drifft.nn.costfunction;

import com.drifft.nn.network.Types.CostFunctionType;

public class MeanSquaredError implements CostFunction {

	public MeanSquaredError() {
	}
	
	public double getErrorValue(double yhat, double y) {	
		return Math.pow((y - yhat), 2);
	}
		
	public double getErrorDerivativeWrtActivatedValue(double yhat, double y) {
		return -2.0 * getErrorValue(yhat, y);
	}

	public CostFunctionType getCostFunctionType() {
		return CostFunctionType.MSE;
	}
}
