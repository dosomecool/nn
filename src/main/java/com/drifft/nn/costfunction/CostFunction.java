package com.drifft.nn.costfunction;

import com.drifft.nn.network.Types.CostFunctionType;

public interface CostFunction {
	public double getErrorValue(double yhat, double y);
	public double getErrorDerivativeWrtActivatedValue(double yhat, double y);
	public CostFunctionType getCostFunctionType();
}
