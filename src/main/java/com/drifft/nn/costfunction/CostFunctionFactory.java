package com.drifft.nn.costfunction;

import com.drifft.nn.network.Types.CostFunctionType;

public class CostFunctionFactory {
 
	// A cost function must satisfy two properties:
	//
	// 1. The cost function C must be able to be written as an average
	//
	//    C = 1 / n Sum( C_i ) 
	// 
	// 2. The cost function C must not be dependent on any activation values 
	//    of a neural network besides the output values OA.
	// 
	// Source: 
	// https://stats.stackexchange.com/questions/154879/a-list-of-cost-functions-used-in-neural-networks-alongside-applications
	
	public CostFunctionFactory() {
	}
	
	public CostFunction get(CostFunctionType type) {

		if(type == null){
			return null;
		}

		if(type.equals(CostFunctionType.MSE)) {
			return new MeanSquaredError();
			
		} else if(type.equals(CostFunctionType.MAD)) {
			return new MeanAbsoluteValue();
			
		} else if(type.equals(CostFunctionType.CROSS_ENTROPY)) {
			return new CrossEntropy();
		}
		return null;
	}
}
