package com.drifft.nn.costfunction;

import com.drifft.nn.network.Types.CostFunctionType;

public class CrossEntropy implements CostFunction {

	// https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Symmetrised_divergence

	// https://ml-cheatsheet.readthedocs.io/en/latest/loss_functions.html

	public CrossEntropy() {
	}

	public double getErrorValue(double yhat, double y) {

		if (y == 1) {
			return -Math.log(yhat);
		}
		return -Math.log(1d - yhat);
	}

	public double getErrorDerivativeWrtActivatedValue(double yhat, double y) {
		return yhat - y;
	}

	public CostFunctionType getCostFunctionType() {
		return CostFunctionType.CROSS_ENTROPY;
	}
}
