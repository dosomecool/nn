package com.drifft.nn.costfunction;

import com.drifft.nn.network.Types.CostFunctionType;

public class MeanAbsoluteValue implements CostFunction {

	public MeanAbsoluteValue() {
	}
	
	public double getErrorValue(double yhat, double y) {		
		return Math.abs( y - yhat );
	}

	public double getErrorDerivativeWrtActivatedValue(double yhat, double y) {
		return (yhat - y) / getErrorValue(yhat, y);
	}

	public CostFunctionType getCostFunctionType() {
		return CostFunctionType.MAD;
	}
}
