package com.drifft.nn.normalization;

import java.util.List;
import java.util.Map;

public class MeanNormalization {

	private String header;

	public MeanNormalization(String header) {
		this.header = header;
	}

	private double min = Double.MAX_VALUE;
	private double max = Double.MIN_VALUE;
	private double mean;

	private boolean isNormalized = false;

	public List<Map<String, Object>> normalizeData(List<Map<String, Object>> data, List<Map<String, Object>> normalizedData) {
		double sum = 0;

		for(int i=0; i<data.size(); i++) {
			double cell = (double) data.get(i).get(header); 
			sum = sum + cell;

			if (cell > max) {
				max = cell;
			}

			if (cell < min) {
				min = cell;
			}
		}
		mean = sum / data.size();
				
		for(int i=0; i<data.size(); i++) {
			double cell = ((double) data.get(i).get(header) - mean) / (max - min);
			normalizedData.get(i).put(header, cell);
		}
		isNormalized = true; 
		return normalizedData;
	}
	
	public double[] denormalize(double[] normalizedColumn) {
		double[] column = new double[normalizedColumn.length];

		for (int i = 0; i < normalizedColumn.length; i++) {
			column[i] = normalizedColumn[i] * (max - min) + mean;
		}
		return column;
	}

	public String getHeader() {
		return header;
	}

	public boolean isNormalized() {
		return isNormalized;
	}
}
