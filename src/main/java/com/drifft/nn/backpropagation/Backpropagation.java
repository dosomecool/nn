package com.drifft.nn.backpropagation;

public interface Backpropagation {

	public int getBatch(); 
	public int getEpochs();
	public void backpropagate();
	public void update(boolean update); 
}
