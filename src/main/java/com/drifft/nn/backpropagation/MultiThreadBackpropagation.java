package com.drifft.nn.backpropagation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.drifft.nn.network.FeedForward;
import com.drifft.nn.network.Layer;
import com.drifft.nn.network.NetworkInput.Parameter;
import com.drifft.nn.network.Types.OptimizationType;
import com.drifft.nn.optimization.OptimizationFactory;
import com.drifft.nn.optimization.Optimizer;

// Futures
// Source: https://www.baeldung.com/java-executor-service-tutorial

public class MultiThreadBackpropagation implements Backpropagation {

	// Batch
	private final int batch;

	// Epochs
	private final int epochs; 

	// Optimization algorithm
	private final Optimizer optimizer;

	// Layers
	private final Layer inputLayer;
	private final ArrayList<Layer> hiddenLayersList;
	private final Layer outputLayer;

	// Executor service 	
	private final ExecutorService executorService;

	public MultiThreadBackpropagation(BackpropagationBuilder builder) {
		this.batch = builder.batch;
		this.epochs = builder.epochs;
		this.optimizer = builder.optimizer;
		this.inputLayer = builder.inputLayer;
		this.hiddenLayersList = builder.hiddenLayersList;
		this.outputLayer = builder.outputLayer;
		this.executorService = builder.executorService; 
	}

	public static class BackpropagationBuilder {
		private int batch = 5;
		private int epochs = 25;
		private Optimizer optimizer;
		private Layer inputLayer;
		private ArrayList<Layer> hiddenLayersList;
		private Layer outputLayer;
		private ExecutorService executorService;
		int index = 0;

		public BackpropagationBuilder(FeedForward feedForward) {

			if (!feedForward.isValid()) {
				throw new IllegalStateException("The feed-forward instance is not valid!");
			}
			this.inputLayer = feedForward.getInputLayer();
			this.hiddenLayersList = feedForward.getHiddenLayersList();
			this.outputLayer = feedForward.getOutputLayer();
		}

		public BackpropagationBuilder setBatch(int batch) {

			if (batch <= 0) {
				throw new IllegalStateException("The training batch must be greater than zero!");
			}
			this.batch = batch;
			return this;
		}

		public BackpropagationBuilder setEpochs(int epochs) {
			if (epochs <= 0) {
				throw new IllegalStateException("The training epochs must be greater than zero!");
			}
			this.epochs = epochs;
			return this;
		}

		public BackpropagationBuilder setOptimizer(OptimizationType optimizationType, Parameter... parameters) {				
			this.optimizer = new OptimizationFactory().get(optimizationType);

			List<Parameter> parameterList = Arrays.asList(parameters);

			if(parameterList != null && parameterList.size() > 0) {
				this.optimizer.setParameters(parameterList);		
			}
			return this;
		}

		public BackpropagationBuilder setThreads(final int threads) {
			this.executorService = Executors.newFixedThreadPool(threads, r -> {

				Thread t = new Thread(r, "backprop-thread-" + String.valueOf(index));
				t.setDaemon(true);
				index = index + 1;
				return t;
			});
			return this;
		}

		public BackpropagationBuilder setOptimizer(OptimizationType optimizationType, List<Parameter> parameterList) {				
			this.optimizer = new OptimizationFactory().get(optimizationType);

			if(parameterList != null && parameterList.size() > 0) {
				this.optimizer.setParameters(parameterList);		
			}
			return this;
		}

		public MultiThreadBackpropagation build() {			
			return new MultiThreadBackpropagation(this);
		}
	}

	// This method calculates all the individual partial differential equations
	private synchronized void calculatePartialDerivatives() {
		List<Callable<Object>> derivativeCalculations = new ArrayList<>();

		// Layer 
		for (int m=0; m<hiddenLayersList.size(); m++) {

			// Node
			for(int n=0; n<hiddenLayersList.get(m).getNodeList().size(); n++) {

				int layer = m;
				int node = n;

				derivativeCalculations.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						// dHA/dH
						hiddenLayersList.get(layer).getNodeList().get(node).calculateActivationDerivativeWrtNode();
						//dH/DHA
						hiddenLayersList.get(layer).getNodeList().get(node).calculateNodeDerivativeWrtActivatedNode();	
						// dH/dW - Pass previous layer
						if (layer==0){
							hiddenLayersList.get(layer).getNodeList().get(node).calculateNodeDerivativeWrtWeight(inputLayer);
						} else {
							hiddenLayersList.get(layer).getNodeList().get(node).calculateNodeDerivativeWrtWeight(hiddenLayersList.get(layer-1));
						}
					}
				}));
			}
		}	

		// Output layer
		for (int k=0; k<outputLayer.getNodeList().size(); k++) {

			int node = k;

			derivativeCalculations.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					// de/dOA
					outputLayer.getNodeList().get(node).calculateErrorDerivativeWrtOutputNode(outputLayer.getNodeList().size()); // removed: outputLayer.getNodeList().size()

					if(!outputLayer.isConnected()) {

						// dOA/dO
						outputLayer.getNodeList().get(node).calculateActivationDerivativeWrtNode();
					} else {

						// dOA/dO other nodes in the output layer
						outputLayer.getNodeList().get(node).calculateActivationDerivativeWrtNodeAndOtherNodes(outputLayer);
					}

					// dO/dHA
					outputLayer.getNodeList().get(node).calculateNodeDerivativeWrtActivatedNode();
					// dO/dW
					outputLayer.getNodeList().get(node).calculateNodeDerivativeWrtWeight(hiddenLayersList.get(hiddenLayersList.size()-1)); // Last hidden layer
				}
			}));
		}

		try {
			executorService.invokeAll(derivativeCalculations);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Calculate input layer connection gradients
	private synchronized void calculateWeightGradients() {		
		List<Callable<Object>> gradientCalculations = new ArrayList<>();

		// Layer
		for(int k=0; k<hiddenLayersList.size(); k++) {

			// Node
			for(int j=0; j<hiddenLayersList.get(k).getNodeList().size(); j++) {

				int layer = k;
				int node = j;

				gradientCalculations.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						// Connections
						for(int i=0; i<hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size(); i++) {

							// Passing values: { layer, node, connection, isOutput}
							double gradientValue = getGradient(layer, node, i, false);
							hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).setGradient( gradientValue );
						}
					}
				}));

				gradientCalculations.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						// Bias
						int biasConnection = hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size();
						// Call gradient for bias weight
						hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().setGradient( getGradient(layer, node, biasConnection, false) );
					}	
				}));
			}
		}

		// Output Nodes
		for(int j=0; j<outputLayer.getNodeList().size(); j++) {

			final int node = j;

			gradientCalculations.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					// Connections
					for(int i=0; i<outputLayer.getNodeList().get(node).getConnectionList().size(); i++){

						// Call get gradient
						outputLayer.getNodeList().get(node).getConnectionList().get(i).setGradient(getGradient(0, node, i, true));
					}
				}
			}));

			gradientCalculations.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					int biasConnection = outputLayer.getNodeList().get(node).getConnectionList().size();
					// Call gradient for bias weight
					outputLayer.getNodeList().get(node).getBiasConnection().setGradient( getGradient(0, node, biasConnection, true) );
				}
			}));
		}

		try {
			executorService.invokeAll(gradientCalculations);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private double getGradient(int layer, int node, int connection, boolean isOutput){

		double product = 1.0;

		// Intermediate hidden layers - including first hidden layer go here
		if (layer < hiddenLayersList.size()-1 && isOutput == false) {

			// dHA/dH
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getActivationDerivativeWrtNode();
			// dH/dW
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection); 
			// Get sum of derivatives from next layer
			product = product * getIntermidiateHiddenLayerDerivativeSumWrtNode(layer, node); // pass current layer

			return product;

			// Last hidden layer
		} else if (layer == hiddenLayersList.size()-1 && isOutput == false) {

			// dHA/dH
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getActivationDerivativeWrtNode();
			// dH/dW
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection); 
			// Get sum of derivatives from output layer

			if(!outputLayer.isConnected()) {
				product = product * getOutputLayerDerivativeSumWrtNode(node);
			} else {
				product = product * getConnectedOutputLayerDerivativeSumWrtNode(node);
			}
			return product;

			// Output layer
		} else if ( isOutput == true ) { 

			// de/dOA
			product = product * outputLayer.getNodeList().get(node).getErrorDerivativeWrtOutputNode();
			// dOA/dO
			product = product * outputLayer.getNodeList().get(node).getActivationDerivativeWrtNode();
			// dO/dHA
			product = product * outputLayer.getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection);

			return product;
		}
		return 1d;
	}

	private double getIntermidiateHiddenLayerDerivativeSumWrtNode(int layer, int node) {

		double sum = 0;

		// Hidden Nodes
		for(int j=0; j<hiddenLayersList.get(layer+1).getNodeList().size(); j++) {

			double product = 1.0;

			// Not last hidden layer
			if( layer == (hiddenLayersList.size()-2) ) { 

				// dH/dHA
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getNodeDerivativeWrtActivatedNode().get(node); 
				// dHA/dH
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getActivationDerivativeWrtNode();

				if(!outputLayer.isConnected()) {
					product = product * getOutputLayerDerivativeSumWrtNode(j);
				} else {
					product = product * getConnectedOutputLayerDerivativeSumWrtNode(j);
				}
				sum = sum + product;

			} else { 

				// dH/dHA
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getNodeDerivativeWrtActivatedNode().get(node); 
				// dHA/dH
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getActivationDerivativeWrtNode();
				// Call "this" method again
				product = product * getIntermidiateHiddenLayerDerivativeSumWrtNode(layer+1, j);

				sum = sum + product;
			} 
		}
		return sum;
	}

	// This returns the total derivative of the output layer w.r.t 
	// to a specific node in the last hidden layer
	private double getOutputLayerDerivativeSumWrtNode(int node) {

		double sum = 0;

		// Node
		for(int m=0; m<outputLayer.getNodeList().size(); m++) {

			double product = 1.0;

			// de/dOA
			product = product * outputLayer.getNodeList().get(m).getErrorDerivativeWrtOutputNode();
			// dOA/dO
			product = product * outputLayer.getNodeList().get(m).getActivationDerivativeWrtNode();
			// dO/dHA
			product = product * outputLayer.getNodeList().get(m).getNodeDerivativeWrtActivatedNode().get(node); // Node from previous layer

			sum = sum + product;
		}
		return sum;
	}

	// This returns the total derivative of the output layer w.r.t 
	// to a specific node in the last hidden layer when the output 
	// layer is fully connected as in the i.e: softmax function
	private double getConnectedOutputLayerDerivativeSumWrtNode(int hiddenNode) {

		double sum = 0;

		// m == Activated Node
		for(int m=0; m<outputLayer.getNodeList().size(); m++) {
			
			double product = 1.0;
			// de/dOA
			product = product * outputLayer.getNodeList().get(m).getErrorDerivativeWrtOutputNode();

			double innerSum = 0;

			// n == Node
			for(int n=0; n<outputLayer.getNodeList().size(); n++) {
				
				double innerProduct = 1.0;

				// dOA/dO				
				innerProduct = innerProduct * outputLayer.getNodeList().get(m).getActivationDerivativeWrtNodeAndOtherNodes(n);
				// dO/dHA
				innerProduct = innerProduct * outputLayer.getNodeList().get(n).getNodeDerivativeWrtActivatedNode().get(hiddenNode); // Node from previous layer

				innerSum = innerSum + innerProduct; 
			}
			sum = sum + product * innerSum;
		}
		return sum;
	}

	// Update connections to hidden layer
	private synchronized void updateHiddenLayerConnectionsWeights() {
		List<Callable<Object>> weightUpdates = new ArrayList<>();

		// Hidden layers
		for(int k=0; k<hiddenLayersList.size() ;k++) {

			// Nodes
			for(int j=0; j<hiddenLayersList.get(k).getNodeList().size(); j++) {

				int layer = k;
				int node = j;

				weightUpdates.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						// Connections
						for(int i=0; i<hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size(); i++) {

							String id       = hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).getId();
							double weight   = hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).getWeight();
							double gradient = hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).getGradient();

							hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).setWeight( optimizer.updateParameter(id, weight, gradient) );
						}
					}
				}));

				weightUpdates.add(Executors.callable(new Runnable() {

					@Override
					public void run() {

						String id       = hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().getId();
						double weight   = hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().getWeight();
						double gradient = hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().getGradient();

						hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().setWeight( optimizer.updateParameter(id, weight, gradient) );
					}
				}));
			}
		}

		try {
			executorService.invokeAll(weightUpdates);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Update connections to output layer
	private synchronized void updateOutputLayerConnectionsWeights() {
		List<Callable<Object>> weightUpdates = new ArrayList<>();

		// Nodes
		for(int j=0; j<outputLayer.getNodeList().size(); j++) {

			int node = j;

			weightUpdates.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					// Connections
					for(int i=0; i<outputLayer.getNodeList().get(node).getConnectionList().size(); i++) {

						String id       = outputLayer.getNodeList().get(node).getConnectionList().get(i).getId();
						double weight   = outputLayer.getNodeList().get(node).getConnectionList().get(i).getWeight();
						double gradient = outputLayer.getNodeList().get(node).getConnectionList().get(i).getGradient();

						outputLayer.getNodeList().get(node).getConnectionList().get(i).setWeight( optimizer.updateParameter(id, weight, gradient) );
					}
				}
			}));

			weightUpdates.add(Executors.callable(new Runnable() {

				@Override
				public void run() {

					String id       = outputLayer.getNodeList().get(node).getBiasConnection().getId();
					double weight   = outputLayer.getNodeList().get(node).getBiasConnection().getWeight();
					double gradient = outputLayer.getNodeList().get(node).getBiasConnection().getGradient();

					outputLayer.getNodeList().get(node).getBiasConnection().setWeight( optimizer.updateParameter(id, weight, gradient) );
				}
			}));
		}
		
		try {
			executorService.invokeAll(weightUpdates);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public synchronized void backpropagate() {
		calculatePartialDerivatives();
		calculateWeightGradients();
		updateHiddenLayerConnectionsWeights();
		updateOutputLayerConnectionsWeights();
	}

	public synchronized void update(boolean update) {
		optimizer.update(update);
	}

	public int getBatch() {
		return batch;
	}

	public int getEpochs() {
		return epochs;
	}
}
