package com.drifft.nn.backpropagation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.drifft.nn.network.FeedForward;
import com.drifft.nn.network.Layer;
import com.drifft.nn.network.NetworkInput.Parameter;
import com.drifft.nn.network.Types.OptimizationType;
import com.drifft.nn.optimization.OptimizationFactory;
import com.drifft.nn.optimization.Optimizer;

// Futures
// Source: https://www.baeldung.com/java-executor-service-tutorial

public class SingleThreadBackpropagation implements Backpropagation {

	// Batch
	private final int batch;

	// Epochs
	private final int epochs; 

	// Optimization algorithm
	private final Optimizer optimizer;

	// Layers
	private final Layer inputLayer;
	private final ArrayList<Layer> hiddenLayersList;
	private final Layer outputLayer;

	public SingleThreadBackpropagation(BackpropagationBuilder builder) {
		this.batch = builder.batch;
		this.epochs = builder.epochs;
		this.optimizer = builder.optimizer;
		this.inputLayer = builder.inputLayer;
		this.hiddenLayersList = builder.hiddenLayersList;
		this.outputLayer = builder.outputLayer;
	}

	public static class BackpropagationBuilder {
		private int batch = 5;
		private int epochs = 25;
		private Optimizer optimizer;
		private Layer inputLayer;
		private ArrayList<Layer> hiddenLayersList;
		private Layer outputLayer;
		

		public BackpropagationBuilder(FeedForward feedForward) {

			if (!feedForward.isValid()) {
				throw new IllegalStateException("The feed-forward instance is not valid!");
			}
			this.inputLayer = feedForward.getInputLayer();
			this.hiddenLayersList = feedForward.getHiddenLayersList();
			this.outputLayer = feedForward.getOutputLayer();
		}

		public BackpropagationBuilder setBatch(int batch) {

			if (batch <= 0) {
				throw new IllegalStateException("The training batch must be greater than zero!");
			}
			this.batch = batch;
			return this;
		}

		public BackpropagationBuilder setEpochs(int epochs) {
			if (epochs <= 0) {
				throw new IllegalStateException("The training epochs must be greater than zero!");
			}
			this.epochs = epochs;
			return this;
		}
		
		public BackpropagationBuilder setOptimizer(OptimizationType optimizationType, Parameter... parameters) {				
			this.optimizer = new OptimizationFactory().get(optimizationType);
			
			List<Parameter> parameterList = Arrays.asList(parameters);
			
			if(parameterList != null && parameterList.size() > 0) {
				this.optimizer.setParameters(parameterList);		
			}
			return this;
		}
		
		public BackpropagationBuilder setOptimizer(OptimizationType optimizationType, List<Parameter> parameterList) {				
			this.optimizer = new OptimizationFactory().get(optimizationType);
			
			if(parameterList != null && parameterList.size() > 0) {
				this.optimizer.setParameters(parameterList);		
			}
			return this;
		}
		
		public SingleThreadBackpropagation build() {			
			return new SingleThreadBackpropagation(this);
		}


	}

	// This method calculates all the individual partial differential equations
	private void calculatePartialDerivatives() {

		// Layer 
		for (int m=0; m<hiddenLayersList.size(); m++) {

			// Node
			for(int n=0; n<hiddenLayersList.get(m).getNodeList().size(); n++) {

				int layer = m;
				int node = n;

				// dHA/dH
				hiddenLayersList.get(layer).getNodeList().get(node).calculateActivationDerivativeWrtNode();
				//dH/DHA
				hiddenLayersList.get(layer).getNodeList().get(node).calculateNodeDerivativeWrtActivatedNode();	
				// dH/dW - Pass previous layer
				if (layer==0){
					hiddenLayersList.get(layer).getNodeList().get(node).calculateNodeDerivativeWrtWeight(inputLayer);
				} else {
					hiddenLayersList.get(layer).getNodeList().get(node).calculateNodeDerivativeWrtWeight(hiddenLayersList.get(layer-1));
				}
			}
		}	

		// Output layer
		for (int k=0; k<outputLayer.getNodeList().size(); k++) {

			int node = k;

			// de/dOA
			outputLayer.getNodeList().get(node).calculateErrorDerivativeWrtOutputNode(outputLayer.getNodeList().size()); // removed: outputLayer.getNodeList().size()

			if(!outputLayer.isConnected()) {

				// dOA/dO
				outputLayer.getNodeList().get(node).calculateActivationDerivativeWrtNode();
			} else {

				// dOA/dO other nodes in the output layer
				outputLayer.getNodeList().get(node).calculateActivationDerivativeWrtNodeAndOtherNodes(outputLayer);
			}

			// dO/dHA
			outputLayer.getNodeList().get(node).calculateNodeDerivativeWrtActivatedNode();
			// dO/dW
			outputLayer.getNodeList().get(node).calculateNodeDerivativeWrtWeight(hiddenLayersList.get(hiddenLayersList.size()-1)); // Last hidden layer
		}
	}

	// Calculate input layer connection gradients
	private void calculateWeightGradients() {		

		// Layer
		for(int k=0; k<hiddenLayersList.size(); k++) {

			// Node
			for(int j=0; j<hiddenLayersList.get(k).getNodeList().size(); j++) {

				int layer = k;
				int node = j;

				// Connections
				for(int i=0; i<hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size(); i++) {

					// Passing values: { layer, node, connection, isOutput}
					double gradientValue = getGradient(layer, node, i, false);
					hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().get(i).setGradient( gradientValue );
				}

				// Bias
				int biasConnection = hiddenLayersList.get(layer).getNodeList().get(node).getConnectionList().size();
				// Call gradient for bias weight
				hiddenLayersList.get(layer).getNodeList().get(node).getBiasConnection().setGradient( getGradient(layer, node, biasConnection, false) );
			}
		}

		// Output Nodes
		for(int j=0; j<outputLayer.getNodeList().size(); j++) {

			final int node = j;

			// Connections
			for(int i=0; i<outputLayer.getNodeList().get(node).getConnectionList().size(); i++){

				// Call get gradient
				outputLayer.getNodeList().get(node).getConnectionList().get(i).setGradient(getGradient(0, node, i, true));
			}

			int biasConnection = outputLayer.getNodeList().get(node).getConnectionList().size();
			// Call gradient for bias weight
			outputLayer.getNodeList().get(node).getBiasConnection().setGradient( getGradient(0, node, biasConnection, true) );
		}
	}

	private double getGradient(int layer, int node, int connection, boolean isOutput){

		double product = 1.0;

		// Intermediate hidden layers - including first hidden layer go here
		if (layer < hiddenLayersList.size()-1 && isOutput == false) {

			// dHA/dH
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getActivationDerivativeWrtNode();
			// dH/dW
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection); 
			// Get sum of derivatives from next layer
			product = product * getIntermidiateHiddenLayerDerivativeSumWrtNode(layer, node); // pass current layer

			return product;

			// Last hidden layer
		} else if (layer == hiddenLayersList.size()-1 && isOutput == false) {

			// dHA/dH
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getActivationDerivativeWrtNode();
			// dH/dW
			product = product * hiddenLayersList.get(layer).getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection); 
			// Get sum of derivatives from output layer

			if(!outputLayer.isConnected()) {
				product = product * getOutputLayerDerivativeSumWrtNode(node);
			} else {
				product = product * getConnectedOutputLayerDerivativeSumWrtNode(node);
			}
			return product;

			// Output layer
		} else if ( isOutput == true ) { 

			// de/dOA
			product = product * outputLayer.getNodeList().get(node).getErrorDerivativeWrtOutputNode();
			// dOA/dO
			product = product * outputLayer.getNodeList().get(node).getActivationDerivativeWrtNode();
			// dO/dHA
			product = product * outputLayer.getNodeList().get(node).getNodeDerivativeWrtWeight().get(connection);

			return product;
		}
		return 1d;
	}

	private double getIntermidiateHiddenLayerDerivativeSumWrtNode(int layer, int node) {

		double sum = 0;

		// Hidden Nodes
		for(int j=0; j<hiddenLayersList.get(layer+1).getNodeList().size(); j++) {

			double product = 1.0;

			// Not last hidden layer
			if( layer == (hiddenLayersList.size()-2) ) { 

				// dH/dHA
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getNodeDerivativeWrtActivatedNode().get(node); 
				// dHA/dH
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getActivationDerivativeWrtNode();

				if(!outputLayer.isConnected()) {
					product = product * getOutputLayerDerivativeSumWrtNode(j);
				} else {
					product = product * getConnectedOutputLayerDerivativeSumWrtNode(j);
				}
				sum = sum + product;

			} else { 

				// dH/dHA
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getNodeDerivativeWrtActivatedNode().get(node); 
				// dHA/dH
				product = product * hiddenLayersList.get(layer+1).getNodeList().get(j).getActivationDerivativeWrtNode();
				// Call "this" method again
				product = product * getIntermidiateHiddenLayerDerivativeSumWrtNode(layer+1, j);

				sum = sum + product;
			} 
		}
		return sum;
	}

	// This returns the total derivative of the output layer w.r.t 
	// to a specific node in the last hidden layer
	private double getOutputLayerDerivativeSumWrtNode(int node) {

		double sum = 0;

		// Node
		for(int m=0; m<outputLayer.getNodeList().size(); m++) {

			double product = 1.0;

			// de/dOA
			product = product * outputLayer.getNodeList().get(m).getErrorDerivativeWrtOutputNode();
			// dOA/dO
			product = product * outputLayer.getNodeList().get(m).getActivationDerivativeWrtNode();
			// dO/dHA
			product = product * outputLayer.getNodeList().get(m).getNodeDerivativeWrtActivatedNode().get(node); // Node from previous layer

			sum = sum + product;
		}
		return sum;
	}

	// This returns the total derivative of the output layer w.r.t 
	// to a specific node in the last hidden layer when the output 
	// layer is fully connected as in the i.e: softmax function
	private double getConnectedOutputLayerDerivativeSumWrtNode(int hiddenNode) {

		double sum = 0;

		// m == Activated Node
		for(int m=0; m<outputLayer.getNodeList().size(); m++) {

//			System.out.println("hiddenNode: " + hiddenNode + " m == Activated Node: " + m);

			double product = 1.0;
			// de/dOA
			product = product * outputLayer.getNodeList().get(m).getErrorDerivativeWrtOutputNode();

			double innerSum = 0;

			// n == Node
			for(int n=0; n<outputLayer.getNodeList().size(); n++) {

//				System.out.println("hiddenNode: " + hiddenNode + " m == Activated Node: " + m + " n == Node: " + n);

				double innerProduct = 1.0;

				// dOA/dO				
				innerProduct = innerProduct * outputLayer.getNodeList().get(m).getActivationDerivativeWrtNodeAndOtherNodes(n);
				// dO/dHA
				innerProduct = innerProduct * outputLayer.getNodeList().get(n).getNodeDerivativeWrtActivatedNode().get(hiddenNode); // Node from previous layer

				innerSum = innerSum + innerProduct; 
			}
			sum = sum + product * innerSum;
		}
		return sum;
	}

	// Update connections to hidden layer
	private void updateHiddenLayerConnectionsWeights() {

		// Hidden layers
		for(int k=0; k<hiddenLayersList.size() ;k++) {

			// Nodes
			for(int j=0; j<hiddenLayersList.get(k).getNodeList().size(); j++) {

				// Connections
				for(int i=0; i<hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().size(); i++) {

					String id       = hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).getId();
					double weight   = hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).getWeight();
					double gradient = hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).getGradient();

					hiddenLayersList.get(k).getNodeList().get(j).getConnectionList().get(i).setWeight( optimizer.updateParameter(id, weight, gradient) );
				}

				String id       = hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().getId();
				double weight   = hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().getWeight();
				double gradient = hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().getGradient();

				hiddenLayersList.get(k).getNodeList().get(j).getBiasConnection().setWeight( optimizer.updateParameter(id, weight, gradient) );
			}
		}
	}

	// Update connections to output layer
	private void updateOutputLayerConnectionsWeights() {

		// Nodes
		for(int j=0; j<outputLayer.getNodeList().size(); j++){

			// Connections
			for(int i=0; i<outputLayer.getNodeList().get(j).getConnectionList().size(); i++) {

				String id       = outputLayer.getNodeList().get(j).getConnectionList().get(i).getId();
				double weight   = outputLayer.getNodeList().get(j).getConnectionList().get(i).getWeight();
				double gradient = outputLayer.getNodeList().get(j).getConnectionList().get(i).getGradient();

				outputLayer.getNodeList().get(j).getConnectionList().get(i).setWeight( optimizer.updateParameter(id, weight, gradient) );
			}

			String id       = outputLayer.getNodeList().get(j).getBiasConnection().getId();
			double weight   = outputLayer.getNodeList().get(j).getBiasConnection().getWeight();
			double gradient = outputLayer.getNodeList().get(j).getBiasConnection().getGradient();

			outputLayer.getNodeList().get(j).getBiasConnection().setWeight( optimizer.updateParameter(id, weight, gradient) );
		}
	}

	public void backpropagate() {
		calculatePartialDerivatives();
		calculateWeightGradients();
		updateHiddenLayerConnectionsWeights();
		updateOutputLayerConnectionsWeights();
	}

	public void update(boolean update) {
		optimizer.update(update);
	}

	public int getBatch() {
		return batch;
	}

	public int getEpochs() {
		return epochs;
	}
}
