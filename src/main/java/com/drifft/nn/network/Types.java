package com.drifft.nn.network;

import java.util.Arrays;
import java.util.List;

public class Types {
	
	public enum NetworkState {
		NETWORK_INITIALIZED, NETWORK_FAILED, NETWORK_FINISHED,
		
		TRAINING_READY, TRAINING_STARTED, TRAINING_FAILED, TRAINING_SUCCEEDED, TRAINING_STOPPED,
		
		VALIDATION_STARTED, VALIDATION_FAILED, VALIDATION_SUCCEEDED,
		
		PREDICTION_STARTED, PREDICTION_FAILED, PREDICTION_SUCCEEDED;
	}
	
	public enum DataType {
		DISCRETE, BINARY, CONTINOUS, MIXED
	}
	
	public enum ConnectionType {
		TO_HIDDEN_NODE, TO_OUTPUT_NODE, TO_BIAS_NODE
	}

	public enum NodeType {
		INPUT, HIDDEN, OUTPUT, BIAS
	}

	public enum ActivationType {
		ELU, RELU, LEAKY_RELU, TANH, SIGMOID, LINEAR, GAUSSIAN, SOFTMAX
	}
	
	public enum NormalizationType {
		MEAN
	}
		
	public enum ParameterType {	
		ETA, ALPHA, BETA;
	}
	
	public enum OptimizationType {
		VANILLA(ParameterType.ETA), MOMENTUM(ParameterType.ALPHA, ParameterType.BETA);
		
		ParameterType[] parameterTypes;
		
		OptimizationType(ParameterType... parameterTypes) {
			this.parameterTypes = parameterTypes;
		}

		public List<ParameterType> getParameterTypes() {
			return Arrays.asList(parameterTypes);
		}
	}

	public enum CostFunctionType {
		MSE, MAD, CROSS_ENTROPY
	}

	public enum ObjectiveType {
		REGRESSION, CLASSIFICATION
	}

	public enum TargetFunctionType {
		LINE, QUADRATIC
	}
}
