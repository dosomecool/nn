package com.drifft.nn.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataLoader {

	// Training data
	private final int inputs;
	private final int outputs;
	private final String[] headers;
	private final List<Map<String, Object>> trainingData;

	private List<Integer> trainingSequence;
	private List<Integer> validationSequence;
	private double trainingPercent;

	// Prediction data
	private List<Map<String, Object>> predictionData;
	private List<Integer> predictionSequence;
		
	private DataLoader(DataLoaderBuilder builder) {
		this.inputs = builder.inputs;
		this.outputs = builder.outputs;
		this.trainingData = builder.trainingData;
		this.headers = builder.headers;
		this.trainingSequence = builder.trainingSequence;
		
		this.validationSequence = builder.validationSequence;
		this.trainingPercent = builder.trainPercent;
		this.predictionData = builder.predictionData;
		this.predictionSequence = builder.predictionSequence;
	}

	public static class DataLoaderBuilder {
		private int inputs;
		private int outputs;
		private String[] headers;
		private List<Map<String, Object>> trainingData;
		private List<Integer> trainingSequence;
		
		private List<Integer> validationSequence;
		private List<Map<String, Object>> predictionData;
		private List<Integer> predictionSequence;
		
		private double trainPercent = 0.75;
		private long seed = new Random().nextLong();
		private boolean shuffle = false;

		public DataLoaderBuilder(int inputs, int outputs, String[] headers,  List<Map<String, Object>> trainingData) {
			this.inputs = inputs;
			this.outputs = outputs;
			this.headers = headers;
			this.trainingData = trainingData;
		}

		public DataLoaderBuilder setTrainPercent(double trainPercent) {
			this.trainPercent = trainPercent;
			return this;
		}

		public DataLoaderBuilder setSeed(long seed) {
			this.seed = seed;
			return this;
		}

		public DataLoaderBuilder setShuffle(boolean shuffle) {
			this.shuffle = shuffle;
			return this;
		}
		
		public DataLoaderBuilder setPredictionData(List<Map<String, Object>> predictionData) {
			this.predictionData = predictionData;
			int start = 0; 
			int end = this.predictionData.size()-1; 
			this.predictionSequence = createIntegerSequence(start, end); 
			return this;
		}

		public DataLoader build() {
			int start = 0; 
			int end = trainingData.size()-1; 
			List<Integer> sequence = createIntegerSequence(start, end);

			if(this.shuffle == true) {
				Collections.shuffle(sequence, new Random(this.seed));
			}
			
			int length = (int) Math.rint( this.trainPercent * sequence.size() );		
			Integer[] tmp = sequence.toArray(new Integer[0]);		
			this.trainingSequence = Arrays.asList(Arrays.copyOfRange(tmp, 0, length));
			this.validationSequence = Arrays.asList(Arrays.copyOfRange(tmp, length, sequence.size()));
			return new DataLoader(this);
		}
	}

	public List<Double> getInputRow(int index) {
		List<Double> inputRow = new ArrayList<>();

		for(int i=0; i<this.inputs; i++) {				
			inputRow.add((double) trainingData.get(index).get(this.headers[i]));
		}
		return inputRow;
	}

	public List<Double> getOutputRow(int index) {		
		List<Double> outputRow = new ArrayList<>();

		for(int i=this.inputs; i<this.inputs+this.outputs; i++) {		
			outputRow.add((double) trainingData.get(index).get(this.headers[i]));
		}		
		return outputRow;
	}
	
	public List<Double> getPredictionRow(int index) {
		List<Double> predictionRow = new ArrayList<>();
		
		for(int i=0; i<this.inputs; i++) {				
			predictionRow.add((double) predictionData.get(index).get(this.headers[i]));
		}
		return predictionRow;
	}
	
	public void setPredictionData(List<Map<String, Object>> predictionData) {
		this.predictionData = predictionData;
		int start = 0; 
		int end = this.predictionData.size()-1; 
		this.predictionSequence = createIntegerSequence(start, end); 
	}
	
	private static List<Integer> createIntegerSequence(final int start, final int end) {
		List<Integer> sequence = IntStream.rangeClosed(start, end)
				.boxed().collect(Collectors.toList());
		return sequence;
	}

	public int getInputs() {
		return inputs;
	}

	public int getOutputs() {
		return outputs;
	}

	public List<Map<String, Object>> getTrainingData() {
		return trainingData;
	}

	public String[] getHeaders() {
		return headers;
	}

	public List<Integer> getTrainingSequence() {
		return trainingSequence;
	}

	public List<Integer> getValidationSequence() {
		return validationSequence;
	}
	
	public List<Integer> getPredictionSequence() {
		return predictionSequence;
	}
	
	public List<Map<String, Object>> getPredictionData() {
		return predictionData;
	}
	
	public double getTrainingPercent() {
		return trainingPercent;
	}

	public void setTrainingPercent(double trainingPercent) {
		this.trainingPercent = trainingPercent;
	}

	public static Double[][] transpose(Double[][] data) {
		int rows = data.length;        
		int cols = data[0].length;
		Double[][] transpose = new Double[cols][rows];

		for (int j=0; j<cols; j++) {
			for (int i=0; i<rows; i++) {
				transpose[j][i] = data[i][j];
			}
		}
		return transpose;
	}
}
