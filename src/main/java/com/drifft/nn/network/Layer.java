package com.drifft.nn.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.nn.network.Types.NodeType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Layer {

	private String id;
	private int index;
	private NodeType nodeType;
	
	private boolean isConnected = false;

	private List<Node> nodeList = new ArrayList<Node>();
	
	public Layer() {
	}
	
	// For input output layer
	public Layer(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	
	// For importing an existing model
	public Layer(String id, int index, NodeType nodeType, List<Node> nodeList) {
		this.id = id;
		this.index = index;
		this.nodeType = nodeType;
		this.nodeList = nodeList;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public NodeType getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}
		
	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public List<Node> getNodeList() {
		return nodeList;
	}
	
	public void setNodeList(List<Node> nodeList) {
		this.nodeList = nodeList;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nindex: ").append(index);
		sb.append("\nnodeType: ").append(nodeType);
		sb.append("\nnodeList: ").append(nodeList);
		
		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();
		
        try {
            jsonStr = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
		return jsonStr;
	}
}
