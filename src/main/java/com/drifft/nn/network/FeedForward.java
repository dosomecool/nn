package com.drifft.nn.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.drifft.nn.activation.ActivationFunction;
import com.drifft.nn.activation.ActivationFunctionFactory;
import com.drifft.nn.costfunction.CostFunction;
import com.drifft.nn.costfunction.CostFunctionFactory;
import com.drifft.nn.costfunction.MeanSquaredError;
import com.drifft.nn.network.Types.ActivationType;
import com.drifft.nn.network.Types.ConnectionType;
import com.drifft.nn.network.Types.CostFunctionType;
import com.drifft.nn.network.Types.NodeType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FeedForward {

	private final String id; 
	private final long seed; 

	@JsonIgnore
	private final Random random;
	private final double variance;
	private final CostFunction costFunction;

	@JsonIgnore
	private final Layer inputLayer = new Layer(NodeType.INPUT);	
	private final ArrayList<Layer> hiddenLayersList = new ArrayList<Layer>();
	private final Layer outputLayer = new Layer(NodeType.OUTPUT);

	@JsonIgnore
	private double error;
	
	private boolean isValid = false;

	private FeedForward(FeedForwardBuilder builder) {
		this.id = builder.id;
		this.seed = builder.seed;
		this.random = new Random(seed);
		this.variance = builder.variance;
		this.costFunction = builder.costFunction;		 
	}

	public static class FeedForwardBuilder {
		private String id = UUID.randomUUID().toString();
		private long seed = 123L; 
		private double variance = 0.2;
		private CostFunction costFunction = new MeanSquaredError();

		public FeedForwardBuilder setSeed(long seed) {
			this.seed = seed;
			return this;
		}

		public FeedForwardBuilder setVariance(double variance) {
			this.variance = variance;		
			return this;
		}

		public FeedForwardBuilder setCostFunction(CostFunctionType type) {
			this.costFunction = new CostFunctionFactory().get(type);
			return this;
		}

		public FeedForward build() {
			return new FeedForward(this);
		}
	}

	// *** Initialize ***

	// Create input layer
	public FeedForward createInputLayer(Integer inputNodes) {
		
		for(int i=0; i<inputNodes; i++) {
			inputLayer.getNodeList().add(new Node(i));
		}
		return this;
	}

	// Create hidden layer
	public FeedForward createHiddenLayer(int hiddenNodes, ActivationType activationType) {

		// Add new empty layer
		hiddenLayersList.add(new Layer(NodeType.HIDDEN));

		// Set layer index
		int lastlayerIndex = hiddenLayersList.size()-1;
		hiddenLayersList.get(lastlayerIndex).setIndex(lastlayerIndex);
		
		// Add nodes to layer
		for(int i=0; i<hiddenNodes; i++) {
			hiddenLayersList.get(lastlayerIndex).getNodeList().add(new Node(i));
		}
		
		// Set activation function
		ActivationFunction activation = new ActivationFunctionFactory().get(activationType);		
		hiddenLayersList.get(lastlayerIndex).getNodeList().forEach( hiddenNode -> {
			hiddenNode.setActivationFunction(activation);
		});	
		return this;
	}

	// Create output layer for classification
	public FeedForward createOutputLayer(int outputNodes, ActivationType activationType) {
		
		for(int i=0; i<outputNodes; i++) {
			outputLayer.getNodeList().add(new Node(i));
		}
		
		// Set activation function
		ActivationFunction activation = new ActivationFunctionFactory(this.outputLayer).get(activationType);
		
//		ActivationFunction activation = new ActivationFunctionFactory().get(activationType);	
		
		outputLayer.getNodeList().forEach( outputNode -> {
			outputNode.setActivationFunction(activation);
		});	

		// Set cost function
		outputLayer.getNodeList().forEach( outputNode -> {
			outputNode.setCostFunction(costFunction);
		});		
		return this;
	}

	// This method must be called last in the chain
	public void createConnections() {

		if (inputLayer.getNodeList().size() == 0) {
			throw new IllegalStateException("The input layer is not created!");
		}
		
		if (hiddenLayersList.get(0).getNodeList().size() == 0) {
			throw new IllegalStateException("The hidden layer is not created!");
		}
		
		if (outputLayer.getNodeList().size() == 0) {
			throw new IllegalStateException("The output layer is not created!");
		}
		
		// Input-Hidden
		for(int i=0; i<inputLayer.getNodeList().size(); i++) {

			int from = inputLayer.getNodeList().get(i).getIndex();

			for(int j=0; j<hiddenLayersList.get(0).getNodeList().size(); j++) {

				int to = hiddenLayersList.get(0).getNodeList().get(j).getIndex();
				Connection c = new Connection(ConnectionType.TO_HIDDEN_NODE, from, to, getRandomWeight());
				hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().add(c);
			}
		}

		// Hidden-Hidden
		for (int k=0; k<hiddenLayersList.size()-1; k++) {

			for(int i=0; i<hiddenLayersList.get(k).getNodeList().size(); i++) {

				int from = hiddenLayersList.get(k).getNodeList().get(i).getIndex();

				for(int j=0; j<hiddenLayersList.get(k+1).getNodeList().size(); j++) { 

					int to = hiddenLayersList.get(k+1).getNodeList().get(j).getIndex();
					Connection c = new Connection(ConnectionType.TO_HIDDEN_NODE, from, to, getRandomWeight());
					hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().add(c);
				}
			}
		}

		// Bias-Hidden
		for (int i=0; i<hiddenLayersList.size(); i++) {

			int from = hiddenLayersList.get(i).getNodeList().size();

			for (int j=0; j<hiddenLayersList.get(i).getNodeList().size(); j++) {

				int to = hiddenLayersList.get(i).getNodeList().get(j).getIndex();
				Connection c = new Connection(ConnectionType.TO_BIAS_NODE, from, to, getRandomWeight());
				hiddenLayersList.get(i).getNodeList().get(j).setBiasConnection(c);
			}
		}

		// Hidden-Output
		for(int i=0; i<hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().size(); i++) {

			int from = hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().get(i).getIndex();

			for(int j=0; j<outputLayer.getNodeList().size(); j++) {

				int to = outputLayer.getNodeList().get(j).getIndex();
				Connection c = new Connection(ConnectionType.TO_BIAS_NODE, from, to, getRandomWeight());
				outputLayer.getNodeList().get(j).getConnectionList().add(c);
			}
		}

		int from = outputLayer.getNodeList().size();

		// Bias-Output
		for(int j=0; j<outputLayer.getNodeList().size(); j++) {

			int to = outputLayer.getNodeList().get(j).getIndex();
			Connection c = new Connection(ConnectionType.TO_BIAS_NODE, from, to, getRandomWeight());
			outputLayer.getNodeList().get(j).setBiasConnection(c);
		}
		this.isValid = true; 
	}

	// Sample from a Normal distribution
	private double getRandomWeight() {
		return random.nextGaussian() * variance;
	}

	// *** End Initialize ***

	public List<Double> getWeights(boolean isGradient) {

		List<Double> weightList = new ArrayList<Double>(); 
		List<Double> gradientList = new ArrayList<Double>(); 

		// Input-Hidden
		for(int i=0; i<inputLayer.getNodeList().size(); i++) {

			for(int j=0; j<hiddenLayersList.get(0).getNodeList().size(); j++) {
				weightList.add( hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().get(i).getWeight() );
				gradientList.add( hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().get(i).getGradient() );
			}
		}

		// Hidden-Hidden
		for (int k=0; k<hiddenLayersList.size()-1; k++) {

			for(int i=0; i<hiddenLayersList.get(k).getNodeList().size(); i++) {

				for(int j=0; j<hiddenLayersList.get(k+1).getNodeList().size(); j++) {
					weightList.add( hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().get(i).getWeight() );
					gradientList.add( hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().get(i).getGradient());
				}
			}
		}

		// Bias-Hidden
		for (int i=0; i<hiddenLayersList.size(); i++){

			for (int j=0; j<hiddenLayersList.get(i).getNodeList().size(); j++){
				weightList.add( hiddenLayersList.get(i).getNodeList().get(j).getBiasConnection().getWeight() );
				gradientList.add( hiddenLayersList.get(i).getNodeList().get(j).getBiasConnection().getGradient() );
			}
		}

		// Hidden-Output
		for(int i=0; i<hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().size(); i++) {

			for(int j=0; j<outputLayer.getNodeList().size(); j++) {
				weightList.add( outputLayer.getNodeList().get(j).getConnectionList().get(i).getWeight() );
				gradientList.add( outputLayer.getNodeList().get(j).getConnectionList().get(i).getGradient() );
			}
		}

		// Bias-Output
		for(int j=0; j<outputLayer.getNodeList().size(); j++){
			weightList.add( outputLayer.getNodeList().get(j).getBiasConnection().getWeight() );
			gradientList.add( outputLayer.getNodeList().get(j).getBiasConnection().getGradient() );
		}
		if(isGradient) return gradientList; 
		return weightList;
	}

	public List<Double> setWeights(List<Double> weightList) { 

		int index = 0;

		// Input-Hidden
		for(int i=0; i<inputLayer.getNodeList().size(); i++) {

			for(int j=0; j<hiddenLayersList.get(0).getNodeList().size(); j++) {
				hiddenLayersList.get(0).getNodeList().get(j).getConnectionList().get(i).setWeight( weightList.get(index) );
				index = index + 1;
			}
		}

		// Hidden-Hidden
		for (int k=0; k<hiddenLayersList.size()-1; k++) {

			for(int i=0; i<hiddenLayersList.get(k).getNodeList().size(); i++) {

				for(int j=0; j<hiddenLayersList.get(k+1).getNodeList().size(); j++){
					hiddenLayersList.get(k+1).getNodeList().get(j).getConnectionList().get(i).setWeight( weightList.get(index) );
					index = index + 1;
				}
			}
		}

		// Bias-Hidden
		for (int i=0; i<hiddenLayersList.size(); i++) {

			for (int j=0; j<hiddenLayersList.get(i).getNodeList().size(); j++){
				hiddenLayersList.get(i).getNodeList().get(j).getBiasConnection().setWeight( weightList.get(index) );
				index = index + 1;
			}
		}

		// Hidden-Output
		for(int i=0; i<hiddenLayersList.get(hiddenLayersList.size()-1).getNodeList().size(); i++) {

			for(int j=0; j<outputLayer.getNodeList().size(); j++) {
				outputLayer.getNodeList().get(j).getConnectionList().get(i).setWeight( weightList.get(index) ) ;
			}
		}

		// Bias-Output
		for(int j=0; j<outputLayer.getNodeList().size(); j++){
			outputLayer.getNodeList().get(j).getBiasConnection().setWeight( weightList.get(index) );
			index = index + 1;
		}
		return weightList;
	}

	// *** Feed Forward *** 

	// Set input node values
	private FeedForward setInputNodeValues(List<Double> data) { 
		
		inputLayer.getNodeList().forEach( inputNode -> {			
			inputNode.setValue( data.get(inputNode.getIndex()) );
		});
		return this;
	}

	// Set hidden node values
	// Activate hidden node values
	private FeedForward setHiddenNodeValues() {

		// First hidden layer
		hiddenLayersList.get(0).getNodeList().forEach( hiddenNode ->{
			// Pass input layer to first hidden layer
			hiddenNode.calculateValue(inputLayer);
			hiddenNode.calculateActivatedValue();
		});

		// Deeper hidden layers
		for(int i=1; i<hiddenLayersList.size(); i++) {

			int index = i;
			hiddenLayersList.get(i).getNodeList().forEach( hiddenNode ->{
				// Pass previous hidden layer to current hidden layer
				hiddenNode.calculateValue(hiddenLayersList.get(index-1));
				hiddenNode.calculateActivatedValue();
			});
		}
		return this;
	}

	// Set output node values
	private FeedForward setOutputNodeValues() {

		outputLayer.getNodeList().forEach( outputNode -> {
			// Pass last hidden layer to output layer
			outputNode.calculateValue(hiddenLayersList.get(hiddenLayersList.size()-1));
			outputNode.calculateActivatedValue();
		});
		return this;
	}

	// Set desired output node values
	private FeedForward setExpectedOutputNodeValues(List<Double> data) {
		
		outputLayer.getNodeList().forEach( outputNode -> {			
			outputNode.setDesiredValue( data.get(outputNode.getIndex()) );
		});
		return this;
	}

	// Calculate error for each output node 
	public double calculateOutputLayerError() {
		double sum = 0.0;
		
		outputLayer.getNodeList().forEach( outputNode -> {
			outputNode.calculateError();
		});	
		for(Node outputNode : outputLayer.getNodeList()) {
			sum = sum + outputNode.getError();
		}
		// Total error is not used in calculations
		return  ( sum / outputLayer.getNodeList().size() ); 
	}

	// Use this to train
	public void feedForward(List<Double> inputRow, List<Double> outputRow) {				
		setInputNodeValues( inputRow )
		.setHiddenNodeValues()
		.setOutputNodeValues()
		.setExpectedOutputNodeValues( outputRow )
		.calculateOutputLayerError();
	}

	// Use this to validate or predict
	public List<Double> feedForward(List<Double> inputRow) {			
		setInputNodeValues( inputRow )
		.setHiddenNodeValues()
		.setOutputNodeValues();

		List<Double> output = new ArrayList<>();

		outputLayer.getNodeList().forEach( outputNode -> {
			output.add(outputNode.getActivatedValue());
		});
		return output;
	}

	public String getId() {
		return id;
	}

	public long getSeed() {
		return seed;
	}

	public double getVariance() {
		return variance;
	}
	
	public CostFunction getCostFunction() {
		return costFunction;
	}

	public Layer getInputLayer() {
		return inputLayer;
	}

	public ArrayList<Layer> getHiddenLayersList() {
		return hiddenLayersList;
	}

	public Layer getOutputLayer() { 
		return outputLayer;
	}

	public double getError() {
		return error;
	}
	
	public boolean isValid() {
		return isValid;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nseed: ").append(seed);
		sb.append("\nvariance: ").append(variance);
		sb.append("\ncostFunction: ").append(costFunction);
		sb.append("\ninputLayer: ").append(inputLayer);
		sb.append("\nhiddenLayersList: ").append(hiddenLayersList);
		sb.append("\noutputLayer: ").append(outputLayer);
		sb.append("\nisValid: ").append(isValid);

		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();

		try {
			jsonStr = mapper.writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonStr;
	}
}
