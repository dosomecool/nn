package com.drifft.nn.network;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import com.drifft.nn.backpropagation.Backpropagation;
import com.drifft.nn.network.Types.NetworkState;

public class Network implements ServiceProvider {

	// Main network components
	private FeedForward feedForward;
	private Backpropagation backpropagation;
	private DataLoader data;

	// Network temporary state
	private NetworkState state;

	// Controls
	private final AtomicBoolean atomicStop = new AtomicBoolean(false);

	// Observer set and sync
	private Set<NetworkService> observers; 
	private final Object MONITOR = new Object();

	// Training temporary parameters
	private int iteration = 0; 
	private double epochError;
	private double batchError;
	private String trainingErrorMessage;

	// Validation temporary parameters
	private double validationProgress = 0.0;
	private int validationIndex;
	private final List<Double> validationOutputList = new ArrayList<>();
	private double validationError;

	// Prediction temporary parameters
	private double predictionProgress = 0.0;
	private int predictionIndex;
	private final List<Double> predictionOutputList = new ArrayList<>();

	private final List<Double> epochErrorList = new ArrayList<>();
	private final List<Double> validationErrorList = new ArrayList<>();

	private ExecutorService executorService = Executors.newFixedThreadPool(1, r -> {
		Thread t = new Thread(r, "network-thread");
		t.setDaemon(true);
		return t;
	});

	public Network(DataLoader data, FeedForward feedForward, Backpropagation backpropagation) {
		this.data = data;
		this.feedForward = feedForward;
		this.backpropagation = backpropagation;
		this.state = NetworkState.NETWORK_INITIALIZED;
	}
	
	// Network tasks
	private MonitoredTask<Void> trainingTask; 
	private MonitoredTask<Void> validationTask;
	private MonitoredTask<Void> predictionTask;

//	private int trainIndex = 0;
	
	public void train() {

		if(iteration == getTotalIterations()) {
			iteration = 0;
		}
		
		trainingTask = new MonitoredTask<Void>("training-task") {

			@Override
			public void calculate() {
				int batch = backpropagation.getBatch();
				int batchIndex = 0;
				for(int i=0; i<backpropagation.getEpochs(); i++) {
					
					for(int index : data.getTrainingSequence()) {

						feedForward.feedForward(data.getInputRow(index), data.getOutputRow(index));				
						backpropagation.backpropagate();

						backpropagation.update(false);
						batchIndex = batchIndex + 1;

						if(batchIndex == batch) {
							backpropagation.update(true);
							batchIndex = 0;
						}

						if(batchIndex == 1) {
							batchError = feedForward.calculateOutputLayerError();							
							epochErrorList.add(batchError);
							notifyBatchErrorChange();
						}

						if(atomicStop.get()) {
							break;
						}
						iteration = iteration + 1;
						notifyIterationChange();
					}

					if(atomicStop.get()) {						
						break;
					}
					epochError = calculateEpochError(epochErrorList);
					notifyEpochErrorChange();
				}
			}

			@Override
			public void onStarted() {
				state = NetworkState.TRAINING_STARTED;
				notifyStateChange();
			}

			@Override
			public void onFinished() {

				if(atomicStop.get()) {
					state = NetworkState.TRAINING_STOPPED;
					notifyStateChange();
				} 

				if(!atomicStop.get() && trainingTask.hasSucceeded()) {
					state = NetworkState.TRAINING_SUCCEEDED;
					notifyStateChange();
				}

				if(!trainingTask.hasSucceeded()) {
					state = NetworkState.TRAINING_FAILED;
					trainingErrorMessage = this.getError();
					notifyStateChange();
				}
				atomicStop.set(false);
			}
		};
		executorService.submit(trainingTask);
	} 

	public void validate() {

		this.validationProgress = 0.0;

		validationTask = new MonitoredTask<Void>("validation-task") {

			@Override
			public void calculate() {

				for(int index : data.getValidationSequence()) {

					feedForward.feedForward(data.getInputRow(index), data.getOutputRow(index));

					validationIndex = index;
					validationOutputList.clear();
					feedForward.getOutputLayer().getNodeList().forEach( outputNode -> {
						validationOutputList.add(outputNode.getActivatedValue());
					});
					notifyValidationOutputChange();
					validationErrorList.add(feedForward.calculateOutputLayerError());			
					validationProgress = validationProgress + 1;
					notifyValidationProgressChange();
				}
				validationError = calculateEpochError(validationErrorList);
				notifyValidationErrorChange();
			}

			@Override
			public void onStarted() {
				state = NetworkState.VALIDATION_STARTED;
				notifyStateChange();
			}

			@Override
			public void onFinished() {

				if(validationTask.hasSucceeded()) {
					state = NetworkState.VALIDATION_SUCCEEDED;
					notifyStateChange();
				}

				if(!validationTask.hasSucceeded()) {
					state = NetworkState.VALIDATION_FAILED;
					notifyStateChange();
				}
			}
		};
		Future<String> future = executorService.submit(validationTask);
	}

	public void predict() {

		this.predictionProgress = 0.0;

		predictionTask = new MonitoredTask<Void>("prediction-task") {

			@Override
			public void calculate() {

				for(int index : data.getPredictionSequence()) {
					
					feedForward.feedForward(data.getPredictionRow(index));

					predictionIndex = index;
					predictionOutputList.clear();
					feedForward.getOutputLayer().getNodeList().forEach( outputNode -> {
						predictionOutputList.add(outputNode.getActivatedValue());
					});
					notifyPredictionOutputChange();		
					predictionProgress = predictionProgress + 1;
					notifyPredictionProgressChange();
				}
			}

			@Override
			public void onStarted() {
				state = NetworkState.PREDICTION_STARTED;
				notifyStateChange();
			}

			@Override
			public void onFinished() {

				if(predictionTask.hasSucceeded()) {
					state = NetworkState.PREDICTION_SUCCEEDED;
					notifyStateChange();
				}

				if(!predictionTask.hasSucceeded()) {
					state = NetworkState.PREDICTION_FAILED;
					notifyStateChange();
				}
			}
		};
		Future<String> future = executorService.submit(predictionTask);
	}

	private double calculateEpochError(final List<Double> errorList) {
		double sum = 0.0;
		double epochError;

		for(double error : errorList) {
			sum = sum + error;
		}
		epochError = sum / errorList.size();
		errorList.clear();
		return epochError;
	}

	// TODO: Fix me
	public void gradientChecking() {
		double epsilon = 0.0001;

		List<Double> originalWeightList = feedForward.getWeights(false);
		List<Double> weightListPlusEpsilon = new ArrayList<>();

		originalWeightList.forEach( weight -> {
			weightListPlusEpsilon.add(weight + epsilon);
			weightListPlusEpsilon.add(weight - epsilon);
		});

		feedForward.setWeights(weightListPlusEpsilon); 
	}

	public boolean isStopped() {
		return atomicStop.get();
	}
	
	public void finish() {
		
//		executorService.
		
	}

	// *** Services ***

	@Override
	public void registerObserver(NetworkService observer) {

		if (observer == null) {			
			return;
		}

		synchronized(MONITOR) {

			if (observers == null) {
				observers = new HashSet<>(1);
			}

			if (observers.add(observer)) { // add other conditions here if necessary
				observer.registerStateService(state);
				observer.registerIterationService();
				observer.registerBatchErrorService();
				observer.registerEpochErrorService();
				
				observer.registerValidationProgressService();
				observer.registerValidationOutputService();
				observer.registerValidationErrorService();
				
				observer.registerPredictionProgressService();
				observer.registerPredictionOutputService();
			}
		}
	}

	@Override
	public void unregisterObserver(NetworkService observer) {

		if (observer == null) {
			return;
		}

		synchronized(MONITOR) {

			if (observers != null && observers.remove(observer) && observers.isEmpty()) {
				// clean up code 
			}
		}
	}

	@Override
	public void notifyStateChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {

			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onStateChange(state);
			}
		}
	}

	@Override
	public void notifyIterationChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {

			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onIterationChange(iteration);
			}
		}
	}

	@Override
	public void notifyBatchErrorChange() {	
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {

			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onBatchErrorChange(batchError);
			}
		}
	}

	public void notifyEpochErrorChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {

			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onEpochErrorChange(epochError);
			}
		}
	}

	@Override
	public void notifyValidationProgressChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {
			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onValidationProgressChange(validationProgress);;
			}
		}
	}

	@Override
	public synchronized void notifyValidationOutputChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {
			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onValidationOutputChange(validationIndex, validationOutputList);
			}
		}
	}

	@Override
	public void notifyValidationErrorChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {
			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onValidationErrorChange(validationError);
			}
		}
	}

	@Override
	public void notifyPredictionProgressChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {
			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onPredictionProgressChange(predictionProgress);;
			}
		}	
	}

	@Override
	public void notifyPredictionOutputChange() {
		Set<NetworkService> observersCopy;

		synchronized(MONITOR) {
			if (observers == null) {			
				return;
			}
			observersCopy = new HashSet<>(observers);

			for (NetworkService observer : observersCopy) { 
				observer.onPredictionOutputChange(predictionIndex, predictionOutputList);
			}
		}
	}

	// *** Getters / Setters *** 

	public int getTotalIterations() {
		return (int) this.data.getTrainingSequence().size() * this.backpropagation.getEpochs();
	}

	public DataLoader getData() {
		return data;
	}

	public FeedForward getFeedForward() {
		return feedForward;
	}

	public Backpropagation getBackPropagation() {
		return backpropagation;
	}

	public AtomicBoolean getAtomicStop() {
		return atomicStop;
	}

	public String getTrainingErrorMessage() {
		return trainingErrorMessage;
	}
}
