package com.drifft.nn.network;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class MonitoredTask<T> implements Callable<String> {

	private String name;
	private String error = "";
	
	private AtomicBoolean success = new AtomicBoolean(false);
	private AtomicBoolean running = new AtomicBoolean(false);
	
	public MonitoredTask(String name) {
		this.name = name;
	}
		
	public abstract void calculate();
	public abstract void onStarted();
	public abstract void onFinished();

	@Override
	public String call() throws Exception {
		running.set(true);
		onStarted();

		try {
			calculate();
			success.set(true);
		} catch (Exception e) {			
			error = e.getMessage();
			success.set(false);
			return error;
		}

		running.set(false);
		onFinished();
		return "no error";
	}
			
	// *** Getters / Setters *** 
		
	public String getName() {
		return name;
	}

	public String getError() {
		return error;
	}

	public boolean isRunning() {
		return running.get();
	}
	
	public boolean hasSucceeded() {
		return success.get();
	}
}
