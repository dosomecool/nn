package com.drifft.nn.network;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.ift.CellProcessor;

import com.drifft.nn.network.Types.ActivationType;
import com.drifft.nn.network.Types.CostFunctionType;
import com.drifft.nn.network.Types.OptimizationType;
import com.drifft.nn.network.Types.ParameterType;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NetworkInput {

	private String trainingFilePath;
	private String[] trainingHeaders;
	private CellProcessor[] trainingProcessors;
	private List<Map<String, Object>> trainingRawData;

	private int inputs;
	private int outputs;

	private double trainingPercent;
	private long shuffleSeed; 
	private boolean shuffle;

	private long feedForwardSeed;
	private double randomWeightVariance;	
	private CostFunctionType costFunctionType;

	private List<HiddenLayer> hiddenLayers;  

	private int outputNodes; 
	private ActivationType outputActivationType;

	private int batch; 
	private int epochs;
	private int threads;
	private OptimizationType optimizationType;
	private List<Parameter> optimizationParameterList;

	public NetworkInput() {
	}

	public NetworkInput(
			String trainingFilePath,
			String[] trainingHeaders,
			CellProcessor[] trainingProcessors,
			List<Map<String, Object>> trainingRawData,
			int inputs,
			int outputs,
			double trainingPercent,
			long shuffleSeed,
			boolean shuffle,
			long feedForwardSeed,
			double randomWeightVariance,	
			CostFunctionType costFunctionType,
			List<HiddenLayer> hiddenLayers,
			int outputNodes,
			ActivationType outputActivationType,
			int batch,
			int epochs,
			int threads,
			OptimizationType optimizationType,
			List<Parameter> optimizationParameterList
			) {
		this.trainingFilePath = trainingFilePath;
		this.trainingHeaders = trainingHeaders;
		this.trainingProcessors = trainingProcessors;
		this.trainingRawData = trainingRawData;

		this.inputs = inputs;
		this.outputs = outputs;

		this.trainingPercent = trainingPercent;
		this.shuffleSeed = shuffleSeed; 
		this.shuffle = shuffle;

		this.feedForwardSeed = feedForwardSeed;
		this.randomWeightVariance = randomWeightVariance; 	
		this.costFunctionType = costFunctionType;

		this.hiddenLayers = hiddenLayers;  

		this.outputNodes = outputNodes; 
		this.outputActivationType = outputActivationType;

		this.batch = batch; 
		this.epochs = epochs;
		this.threads = threads;
		this.optimizationType = optimizationType;
		this.optimizationParameterList = optimizationParameterList;
	}
	
	public String getTrainingFilePath() {
		return trainingFilePath;
	}

	public void setTrainingFilePath(String trainingFilePath) {
		this.trainingFilePath = trainingFilePath;
	}

	public String[] getTrainingHeaders() {
		return trainingHeaders;
	}

	public void setTrainingHeaders(String[] trainingHeaders) {
		this.trainingHeaders = trainingHeaders;
	}

	public CellProcessor[] getTrainingProcessors() {
		return trainingProcessors;
	}

	public void setTrainingProcessors(CellProcessor[] trainingProcessors) {
		this.trainingProcessors = trainingProcessors;
	}

	public List<Map<String, Object>> getTrainingRawData() {
		return trainingRawData;
	}

	public void setTrainingRawData(List<Map<String, Object>> trainingRawData) {
		this.trainingRawData = trainingRawData;
	}

	public int getInputs() {
		return inputs;
	}

	public void setInputs(int inputs) {
		this.inputs = inputs;
	}

	public int getOutputs() {
		return outputs;
	}

	public void setOutputs(int outputs) {
		this.outputs = outputs;
	}

	public double getTrainingPercent() {
		return trainingPercent;
	}

	public void setTrainingPercent(double trainingPercent) {
		this.trainingPercent = trainingPercent;
	}

	public long getShuffleSeed() {
		return shuffleSeed;
	}

	public void setShuffleSeed(long shuffleSeed) {
		this.shuffleSeed = shuffleSeed;
	}

	public boolean isShuffle() {
		return shuffle;
	}

	public void setShuffle(boolean shuffle) {
		this.shuffle = shuffle;
	}

	public long getFeedForwardSeed() {
		return feedForwardSeed;
	}

	public void setFeedForwardSeed(long feedForwardSeed) {
		this.feedForwardSeed = feedForwardSeed;
	}

	public double getRandomWeightVariance() {
		return randomWeightVariance;
	}

	public void setRandomWeightVariance(double randomWeightVariance) {
		this.randomWeightVariance = randomWeightVariance;
	}

	public CostFunctionType getCostFunctionType() {
		return costFunctionType;
	}

	public void setCostFunctionType(CostFunctionType costFunctionType) {
		this.costFunctionType = costFunctionType;
	}

	public List<HiddenLayer> getHiddenLayers() {
		return hiddenLayers;
	}

	public void setHiddenLayers(List<HiddenLayer> hiddenLayers) {
		this.hiddenLayers = hiddenLayers;
	}

	public int getOutputNodes() {
		return outputNodes;
	}

	public void setOutputNodes(int outputNodes) {
		this.outputNodes = outputNodes;
	}

	public ActivationType getOutputActivationType() {
		return outputActivationType;
	}

	public void setOutputActivationType(ActivationType outputActivationType) {
		this.outputActivationType = outputActivationType;
	}

	public int getBatch() {
		return batch;
	}

	public void setBatch(int batch) {
		this.batch = batch;
	}

	public int getEpochs() {
		return epochs;
	}

	public void setEpochs(int epochs) {
		this.epochs = epochs;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}

	public OptimizationType getOptimizationType() {
		return optimizationType;
	}

	public void setOptimizationType(OptimizationType optimizationType) {
		this.optimizationType = optimizationType;
	}

	public List<Parameter> getOptimizationParameterList() {
		return optimizationParameterList;
	}

	public void setOptimizationParameterList(List<Parameter> optimizationParameterList) {
		this.optimizationParameterList = optimizationParameterList;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("trainingFilePath: ").append(trainingFilePath);
		sb.append("\ntrainingHeaders: ").append(trainingHeaders);
		sb.append("\ntrainingProcessors: ").append(trainingProcessors);
		sb.append("\ntrainingRawData: ").append(trainingRawData);
		sb.append("\ninputs: ").append(inputs);
		sb.append("\noutputs: ").append(outputs);
		sb.append("\ntrainingPercent: ").append(trainingPercent);
		sb.append("\nshuffleSeed: ").append(shuffleSeed);
		sb.append("\nshuffle: ").append(shuffle);
		sb.append("\nfeedForwardSeed: ").append(feedForwardSeed);
		sb.append("\nrandomWeightVariance: ").append(randomWeightVariance);
		sb.append("\ncostFunctionType: ").append(costFunctionType);
		sb.append("\nhiddenLayers: ").append(hiddenLayers);
		sb.append("\noutputNodes: ").append(outputNodes);
		sb.append("\noutputActivationType: ").append(outputActivationType);
		sb.append("\nbatch: ").append(batch);
		sb.append("\nepochs: ").append(epochs);
		sb.append("\nthreads: ").append(threads);
		sb.append("\noptimizationType: ").append(optimizationType);
		sb.append("\noptimizationParameterList: ").append(optimizationParameterList);

		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();

		try {
			jsonStr = mapper.writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonStr;
	}

	public static class HiddenLayer {

		private int hiddenNodes;
		private ActivationType activationType;

		public HiddenLayer() {
		}

		public HiddenLayer(int hiddenNodes, ActivationType activationType) {
			this.hiddenNodes = hiddenNodes;
			this.activationType = activationType; 
		}
		
		public int getHiddenNodes() {
			return hiddenNodes;
		}

		public void setHiddenNodes(int hiddenNodes) {
			this.hiddenNodes = hiddenNodes;
		}

		public ActivationType getActivationType() {
			return activationType;
		}

		public void setActivationType(ActivationType activationType) {
			this.activationType = activationType;
		}

		@Override 
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("hiddenNodes: ").append(hiddenNodes);
			sb.append("\nactivationType: ").append(activationType);

			String jsonStr = null; 
			ObjectMapper mapper = new ObjectMapper();

			try {
				jsonStr = mapper.writeValueAsString(this);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return jsonStr;
		}
	}

	public static class Parameter {

		private ParameterType parameterType;
		private Double value;

		public Parameter() {
		}

		public Parameter(ParameterType parameterType, Double value) {
			this.parameterType = parameterType;
			this.value = value;
		}

		public ParameterType getParameterType() {
			return parameterType;
		}

		public void setParameterType(ParameterType parameterType) {
			this.parameterType = parameterType;
		}

		public Double getValue() {
			return value;
		}

		public void setValue(Double value) {
			this.value = value;
		}
		
		@Override 
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("parameterType: ").append(parameterType);
			sb.append("\nvalue: ").append(value);

			String jsonStr = null; 
			ObjectMapper mapper = new ObjectMapper();

			try {
				jsonStr = mapper.writeValueAsString(this);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return jsonStr;
		}
	}
}