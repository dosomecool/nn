package com.drifft.nn.network;

public interface ServiceProvider {
	public void registerObserver(NetworkService observer);
	public void unregisterObserver(NetworkService observer);
	
	// Network state
	public void notifyStateChange();
	
	// Training
	public void notifyIterationChange(); // change this to progress too
	public void notifyEpochErrorChange();
	public void notifyBatchErrorChange();
	
	// Validation
	public void notifyValidationProgressChange();
	public void notifyValidationOutputChange();
	public void notifyValidationErrorChange();
	
	// Prediction
	public void notifyPredictionProgressChange();
	public void notifyPredictionOutputChange();
}
