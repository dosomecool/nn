package com.drifft.nn.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.drifft.nn.activation.ActivationFunction;
import com.drifft.nn.costfunction.CostFunction;
import com.drifft.nn.network.Types.NodeType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Node {

	// Attributes
	private String id;
	private int index;

	// Values
	@JsonIgnore
	private double value = 0;

	@JsonIgnore
	private double activatedValue = 0;

	@JsonIgnore
	private double desiredValue;

	@JsonIgnore
	private double error = 0;

	// Derivatives
	@JsonIgnore
	private double errorDerivativeWrtOutputNode = 0.0;

	@JsonIgnore
	private double activationDerivativeWrtNode  = 0.0;

	@JsonIgnore
	private final List<Double> nodeDerivativeWrtActivatedNode = new ArrayList<Double>();

	@JsonIgnore
	private final List<Double> nodeDerivativeWrtWeight = new ArrayList<Double>();

	// For hidden and output nodes
	private List<Connection> connectionList = new ArrayList<Connection>();
	private Connection biasConnection;
	private double biasNodeValue = 1.0; 

	@JsonIgnore
	private ActivationFunction activationFunction;

	@JsonIgnore
	private CostFunction costFunction;

	public Node() {
	}

	public Node(int index) {
		this.index = index;
	}

	public Node(String id, int index, List<Connection> connectionList, Connection biasConnection) {
		this.id = id;
		this.index = index;
		this.connectionList = connectionList;
		this.biasConnection = biasConnection;
	}

	// *** VALUES ***

	// For hidden and output nodes	
	public void calculateValue(Layer layer) {

		value = 0.0;

		for(int i=0; i<connectionList.size(); i++) {

			double nodeValue;

			if (layer.getNodeType() == NodeType.INPUT){
				nodeValue = layer.getNodeList().get(i).getValue();
			} else {
				nodeValue = layer.getNodeList().get(i).getActivatedValue();
			}
			value = value + nodeValue * connectionList.get(i).getWeight();
		}
		value = value + biasNodeValue * biasConnection.getWeight(); 
	}

	// For hidden and output nodes
	public void calculateActivatedValue() {
		this.activatedValue = activationFunction.getFunctionValue(this.value);
	}

	// For output nodes only
	public void calculateError() {
		error = costFunction.getErrorValue(activatedValue, desiredValue);
	}

	// *** DERIVATIVES ***

	// For output nodes only
	// dE/dOA
	public void calculateErrorDerivativeWrtOutputNode(double outputNodes) {	
		errorDerivativeWrtOutputNode = costFunction.getErrorDerivativeWrtActivatedValue(activatedValue, desiredValue) / outputNodes;
	}

	public double getErrorDerivativeWrtOutputNode() {
		return errorDerivativeWrtOutputNode;
	}

	// For Hidden and output nodes
	// dHA_i / dH_i OR dOA_i / dO_i 
	public void calculateActivationDerivativeWrtNode() {
		activationDerivativeWrtNode = activationFunction.getFunctionDerivativeValue(value);
	}

	public double getActivationDerivativeWrtNode() {
		return activationDerivativeWrtNode;
	}

	// *** new *** 
	
	private List<Double> activationDerivativeWrtOtherNodeList = new ArrayList<>();

	// Lets say we call this before and populate the list
	public void calculateActivationDerivativeWrtNodeAndOtherNodes(Layer outputLayer) {

		activationDerivativeWrtOtherNodeList.clear();
		
		outputLayer.getNodeList().forEach( outputNode -> {
			
			double derivativeValue;
			
			// case i == j
			if(outputNode.getIndex() == index) {
				double xi = this.value;
				derivativeValue = activationFunction.getFunctionDerivativeValue(xi, xi, true);
			
			// case i != j
			} else {
				double xi = this.value;
				double xj = outputNode.getValue();
				derivativeValue = activationFunction.getFunctionDerivativeValue(xi, xj, false);
			}
			activationDerivativeWrtOtherNodeList.add(derivativeValue);
		});
	}
	
	public double getActivationDerivativeWrtNodeAndOtherNodes(int index) {
		return activationDerivativeWrtOtherNodeList.get(index);
	}
	
	// *** new ***

	// For Hidden and output nodes
	// dH/dHA OR dO/dHA
	public void calculateNodeDerivativeWrtActivatedNode() {

		// Clear previous derivatives
		nodeDerivativeWrtActivatedNode.clear();

		connectionList.forEach( connection -> {
			nodeDerivativeWrtActivatedNode.add(connection.getWeight());
		});

		// Bias derivative not required since they  
		// are not part of the derivative chain
	}

	public List<Double> getNodeDerivativeWrtActivatedNode() {
		return nodeDerivativeWrtActivatedNode;
	}

	// For Hidden nodes only
	// dH/dW
	public void calculateNodeDerivativeWrtWeight(Layer previousLayer) {

		nodeDerivativeWrtWeight.clear();

		// For input nodes
		if(previousLayer.getNodeType() == NodeType.INPUT) {

			previousLayer.getNodeList().forEach( inputNode -> {
				nodeDerivativeWrtWeight.add(inputNode.getValue());
			});

			// For hidden nodes
		} else {
			previousLayer.getNodeList().forEach( hiddenNode -> {
				nodeDerivativeWrtWeight.add(hiddenNode.getActivatedValue());
			});
		}

		// For bias nodes
		nodeDerivativeWrtWeight.add(biasNodeValue);
	}

	public List<Double> getNodeDerivativeWrtWeight() {
		return nodeDerivativeWrtWeight;
	}

	public ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	public void setActivationFunction(ActivationFunction activationFunction) {
		this.activationFunction = activationFunction;
	}

	public void setCostFunction(CostFunction costFunction) {
		this.costFunction = costFunction; 
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	// Returns value of the node
	public double getValue() {
		return value;
	}

	// For input nodes only
	public void setValue(double value) {
		this.value = value;
	}

	// Returns activated value of the node
	public double getActivatedValue() {
		return activatedValue;
	}

	public void setActivatedValue(double activatedValue){
		this.activatedValue = activatedValue;
	}

	public double getDesiredValue() {
		return desiredValue;
	}

	// Only for output nodes
	public void setDesiredValue(double desiredValue) {
		this.desiredValue = desiredValue;
	}

	// For output nodes only
	public double getError() {
		return error;
	}

	public List<Connection> getConnectionList() {
		return connectionList;
	}

	public void setConnectionList(List<Connection> connectionList) {
		this.connectionList = connectionList;
	}

	public Connection getBiasConnection() {
		return biasConnection;
	}

	public void setBiasConnection(Connection biasConnection) {
		this.biasConnection = biasConnection;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: ").append(id);
		sb.append("\nindex: ").append(index);
		sb.append("\nconnectionList: ").append(connectionList);
		sb.append("\nbiasConnection: ").append(biasConnection);

		String jsonStr = null; 
		ObjectMapper mapper = new ObjectMapper();

		try {
			jsonStr = mapper.writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonStr;
	}
}
