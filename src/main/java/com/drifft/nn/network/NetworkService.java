package com.drifft.nn.network;

import java.util.List;

import com.drifft.nn.network.Types.NetworkState;

public interface NetworkService {
	
	public void registerObserver();
	public void unregisterObserver();
	
	//Status
	public void registerStateService(NetworkState state);
    public void onStateChange(NetworkState state);
	
	// Training
	public void registerIterationService();
	public void registerBatchErrorService();
	public void registerEpochErrorService();
	
	public void onIterationChange(int iteration);
	public void onBatchErrorChange(double error);
	public void onEpochErrorChange(double error);
	
	// Validation
	public void registerValidationProgressService();
	public void registerValidationOutputService();
	public void registerValidationErrorService();

	public void onValidationProgressChange(double validationProgress);
	public void onValidationOutputChange(int index, List<Double> validationOutputList);
	public void onValidationErrorChange(double error);
	
	// Prediction
	public void registerPredictionProgressService();
	public void registerPredictionOutputService();
	public void registerPredictionErrorService();

	public void onPredictionProgressChange(double predictionProgress);
	public void onPredictionOutputChange(int index, List<Double> predictionOutputList);
}
