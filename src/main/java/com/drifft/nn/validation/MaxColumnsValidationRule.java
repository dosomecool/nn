package com.drifft.nn.validation;

import com.drifft.nn.network.NetworkInput;

public class MaxColumnsValidationRule implements NetworkRule {

	private String error = "";
	
	@Override
	public void validate(NetworkInput userInput) {

		int inputs = userInput.getInputs();
		int outputs = userInput.getOutputs();
		int columns = inputs + outputs;
		String[] headers = userInput.getTrainingHeaders();

		if (columns > headers.length) {
			this.error = "The number of specified inputs and outputs exceeds the max number of data columns!";
		}
	}

	@Override
	public String getError() {
		return !error.equals("") ? error : null;
	}
}
