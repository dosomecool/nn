package com.drifft.nn.validation;

import java.util.ArrayList;
import java.util.List;

import com.drifft.nn.network.NetworkInput;

public class Validator {
	
	private final List<NetworkRule> ruleList = new ArrayList<>();
	
	public Validator(NetworkInput userInput) {
		ruleList.add(new MaxColumnsValidationRule());

		for ( NetworkRule rule : ruleList){
			rule.validate(userInput);
		}
	}

	public List<String> check() {
		List<String> errorList = new ArrayList<>();
		
		for(NetworkRule rule : ruleList) {
			String error;
			
			if((error = rule.getError()) != null) {
				errorList.add(error);
			}
		}
		return null;
	}
}
