package com.drifft.nn.validation;

import com.drifft.nn.network.NetworkInput;

public interface NetworkRule {
	public void validate(NetworkInput userInput);
	public String getError();
}
